<?php
/**
 *
 * This is a template file for single events
 *
 */
class single_events_template_render
{

    private $single_template_id = null;
    private $events_data = null;
    private $post = null;

    public function __construct()
    {

        add_action('init', array($this, 'initialize_template'));

    }

    public function initialize_template()
    {
        $single_template_id = get_option('ectbe_events_addons_single_page_id', false);
        if ($single_template_id != false && 'publish' == get_post_status($single_template_id) && 'ectpse' == get_post_type($single_template_id)) {
            $this->single_template_id = $single_template_id;
            $this->events_data = get_post($this->single_template_id);
        }
    }

    public function render_event_title($attr, $content = null)
    {
        $options = shortcode_atts(array(
            'tag' => 'h1',
            'size' => '24',
        ), $attr);

        global $post;
        $tag = (isset($options['tag']) && $options['tag'] != '') ? $options['tag'] : 'h1';
        $size = (isset($options['size']) && $options['size'] != '') ? $options['size'] . 'px' : '24px';

        $style = 'font-size:' . $size . ';';

        $html = '';
        if ($post->post_title != ''):
            $html .= '<' . $tag . ' style="' . $style . '">' . $post->post_title . '</' . $tag . '>';
        endif;

        return $html;
    }

    public function render_event_all_events_link( $attr, $contet = null )
    {

        $options = shortcode_atts(array(
            'tag' => 'h1',
            'size' => '24',
            'text' => __('All Events', 'the-events-calendar'),
        ), $attr);

        $all_events_op = '';

        if (function_exists('tribe_get_events_link')) {
            $all_events_op .= '<p class="ectbe-tribe-events-back">
                <a href="' . tribe_get_events_link() . '"target="_blank"><button class="tec_allevents">' . __('All Events', 'the-events-calendar') . '</button></a>
                    </p>';
        }

        if (!empty($el_class)) {
            $all_events_op .= '</div>';
        }
        return $all_events_op;

    }

}

$GLOBALS['cool_events_single_template'] = new single_events_template_render();
