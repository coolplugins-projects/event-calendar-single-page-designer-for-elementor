<?php
use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

class Ectbe_widgets_related_events extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

          wp_register_style( 'ectbe-events-related', plugin_dir_url(__FILE__) .'css/ectbe-related-event.css' , '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_style_depends()
    {
        return ['ectbe-events-related'];
    }

    public function get_name()
    {
        return "the-event-related-event";
    }

    public function get_title()
    {
        return "Related Events";
    }

    public function get_icon()
    {
        return 'fas fa-clone';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Related Event Title Settings', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
          	$this->add_control(
			'ectbe_rel_btn_heading',
			[
				'label' => __( 'Title ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        $this->add_control(
            'ectbe_related_custom_title',
            [
                'label' => __('Title', 'ectbe'),
                'type' => \Elementor\Controls_Manager::TEXT,
                'default' => __('Related Events', 'ectbe'),
                'placeholder' => __('Type your title here', 'ectbe'),

            ]
        );

     

        $this->add_control(
            'ectbe_related_title__color',
            [
                'label' => __('Color', 'color'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ectbe-related-head ,
                    {{WRAPPER}} .tribe-events-related-events-title' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'ectbe_related_title_bg_color',
            [
                'label' => __('Background Color', 'color'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '
                    {{WRAPPER}} .ectbe-related-area h3 ,
                    {{WRAPPER}} .tribe-events-related-events-title' => 'background: {{VALUE}}',
                ],
            ]
        );
           $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ectbe_related_typography',
                'label' => __('Typography', 'ectbe'),
                'selector' => '{{WRAPPER}} .ectbe-related-area  h3,
                                    {{WRAPPER}} .tribe-events-related-events-title',
            ]
        );
          $this->add_responsive_control(
            'ectbe_rel_title_padding', //param_name
            [
                'label' => __('Padding', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '
                    {{WRAPPER}} .ectbe-related-area h3 ,
                    {{WRAPPER}} .tribe-events-related-events-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_rel__title_margin', //param_name
            [
                'label' => __('Margin', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                   '
                    {{WRAPPER}} .ectbe-related-area h3 ,
                    {{WRAPPER}} .tribe-events-related-events-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_control(
            'view_related_image_hover',
            [
                'label' => __('View', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'traditional',
                'selectors' => [                   
                 
                    '{{WRAPPER}} .ectbe-row' => 'display: flex;',            
                  
                 
                ],
            ]
        ); 

       	$this->add_control(
			'ectbe_rel_btn_heading_content',
			[
				'label' => __( 'Content ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        $this->add_control(
            'ectbe_related_title_first_color',
            [
                'label' => __('Color', 'color'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ectbe-related-area  span,

                    {{WRAPPER}} .ectbe-related-title a,
                    {{WRAPPER}} .tribe-related-event-info span,
                    {{WRAPPER}} .tribe-related-event-info a' => 'color: {{VALUE}}',
                ],
            ]
        );
               $this->add_control(
            'ectbe_related_tcontent_bg_color',
            [
                'label' => __('Background Color', 'color'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '
                 
                    {{WRAPPER}} .ectbe-light-bg' => 'background: {{VALUE}}',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ectbe_related_typography_title',
                'label' => __('Typography', 'ectbe'),
                'selector' => '{{WRAPPER}} .ectbe-related-area  span ,
                                {{WRAPPER}} .ectbe-related-title h4,
                                    {{WRAPPER}} .ectbe-related-area  h3 a,
                                    {{WRAPPER}} .tribe-related-event-info span,
                    {{WRAPPER}} .tribe-related-event-info a',
            ]
        );

       $this->add_responsive_control(
            'ectbe_featured_image_width',
            [
                'label' => __('Image Width', 'ectbe'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 500,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 150,
                ],
                'selectors' => [
                    '{{WRAPPER}} .tribe-events-event-image img' => 'width: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
    

        $this->add_responsive_control(
            'ectbe_rel_padding', //param_name
            [
                'label' => __('Padding', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => ['
                    {{WRAPPER}} .ectbe-related-box ' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_rel_margin', //param_name
            [
                'label' => __('Margin', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '
                    {{WRAPPER}} .ectbe-related-box
                    ' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_section();

    }

/**
 * Template tag to get related posts for the current post.
 *
 * This function has a private access modifer
 *
 * @param int $count number of related posts to return.
 * @param int|obj $post the post to get related posts to, defaults to current global $post
 *
 * @return array the related posts.
 */
    private function tec_tribe_event_page_get_related_posts($id)
    {
        $post = false;
        $post_id = '';
        $tags = '';
        $count = 2;
        $categories = '';
        if (class_exists('Tribe__Events__Main')) {
            $post_id = $id;

            $tags = wp_get_post_tags($post_id, array('fields' => 'ids'));
            $categories = wp_get_object_terms($post_id, Tribe__Events__Main::TAXONOMY, array('fields' => 'ids'));
            if (!$tags && !$categories) {
                return;
            }
        }
        $args = array(
            'posts_per_page' => $count,
            'post__not_in' => array($post_id),
            'eventDisplay' => 'list',
            'tax_query' => array('relation' => 'OR'),
            'orderby' => 'rand',
        );
        if ($tags) {
            $args['tax_query'][] = array('taxonomy' => 'post_tag', 'field' => 'id', 'terms' => $tags);
        }
        if ($categories) {
            $args['tax_query'][] = array(
                'taxonomy' => Tribe__Events__Main::TAXONOMY,
                'field' => 'id',
                'terms' => $categories,
            );
        }

        $args = apply_filters('tribe_related_posts_args', $args);
        $posts = '';
        if ($args) {
            if (class_exists('Tribe__Events__Query')) {
                $posts = Tribe__Events__Query::getEvents($args);
            }
        } else {
            $posts = array();
        }

        return apply_filters('tribe_get_related_posts', $posts);
    }
    protected function render()
    {

        $single_page_related_events_op = '';
        $id = '';
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
            $posts1= $this->tec_tribe_event_page_get_related_posts($id);
              if(empty($posts1)){
                
                echo'<span class="ectbe-alert-msg"><b>'. __('This widget is displayed if there are related events.', 'ectbe').'</b></span>';


            }
        } else {
            $id = get_the_ID();

        }

        $settings = $this->get_settings_for_display();
        $titlelbl = !empty($settings['ectbe_related_custom_title']) ? $settings['ectbe_related_custom_title'] : 'Related Events';

        $css_class = 'ectbe-related-events';

        if (class_exists('Tribe__Events__Pro__Main') && tribe_get_option('hideRelatedEvents', false) == true) {
            return;
        }

        $posts = $this->tec_tribe_event_page_get_related_posts($id);

        

        if (is_array($posts) && !empty($posts)):
            $single_page_related_events_op .= '<div class="' . esc_attr($css_class) . '">';

            ?>
          
	            <div class="ectbe-related-area">
	                <h3 class="ectbe-related-head"><?php echo __($titlelbl, 'ectbe'); ?></h3>
	                <div class="ectbe-row">
	                    <?php foreach ($posts as $post): ?>
	                    <div >
	                        <div class="ectbe-related-box">
	                            <a href="<?php echo tribe_get_event_link($post); ?>">
	                                <?php echo tribe_event_featured_image($post->ID, 'full', false); ?>
	                            </a>
	                            <div class="ectbe-related-title <?php if (tribe_event_featured_image($post->ID, 'full', false) == '') {echo "no-image";}?>">
	                                <h4><a href="<?php echo tribe_get_event_link($post); ?>"><?php echo esc_html(get_the_title($post->ID)); ?></a></h4>
	                                <div class="ectbe-related-date"><?php echo tribe_events_event_schedule_details($post->ID); ?></div>
	                                <div class="ectbe-light-bg"></div>
	                            </div>
	                        </div>
	                    </div>
	                    <?php endforeach;?>
                </div>
            </div>
        <?php
$single_page_related_events_op .= '</div>';

        endif;

        echo $single_page_related_events_op;

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_related_events());
