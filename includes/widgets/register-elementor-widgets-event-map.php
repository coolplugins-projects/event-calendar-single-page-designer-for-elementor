<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

class Ectbe_widgets_event_map extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }

    public function get_name()
    {
        return "the-event-map";
    }

    public function get_title()
    {
        return "Event Map";
    }

    public function get_icon()
    {
        return 'fas fa-map-marked';
    }

    // register controls
    protected function _register_controls()
    {

       


        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Event Map', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

       

		$this->add_responsive_control(
			'ectbe_map_box_padding', //param_name
			[
				'label' 		=> __('Padding', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-map' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'ectbe_map_box_margin', //param_name
			[
				'label' 		=> __('Margin', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-map' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
        	$this->add_control(
			'nap_hiiden_data',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

        $this->end_controls_section();

    }

    /**
     * Get Meta Map template
     */


    protected function render()
    {

        $single_page_meta_event_map_op = '';
        $id = '';
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
              $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
               if(empty(tribe_get_embedded_map( $id))){
                
                echo'<span class="ectbe-alert-msg">'. __('This widget is displayed if Map is set.', 'ectbe').'</span>';


            }
    
        }else{
            $id = get_the_ID();

        }

      
        $settings = $this->get_settings_for_display();
        $css_class = 'ectbe-event-map';

        
        $single_page_meta_event_map_op .= '<div class="' . esc_attr($css_class) . '">';
        
        if ( class_exists( 'Tribe__Events__Main' ) ) {
            $single_page_meta_event_map_op .= tribe_get_embedded_map( $id);


        }
       
        

        if (!empty($css_class)) {
            $single_page_meta_event_map_op .= '</div>';
        }

        echo $single_page_meta_event_map_op;

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_event_map());
