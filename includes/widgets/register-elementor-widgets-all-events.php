<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Css_Filter;
use Elementor\Group_Control_Typography;


class Ectbe_widgets_all_events extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

      //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }

    public function get_name()
    {
        return "the-event-all-events";
    }

    public function get_title()
    {
        return "All Events Link";
    }

    public function get_icon()
    {
        return 'fa fa-link';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('All events', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
         $this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' 		=> 'all_event_typography',
					'label' 	=> __( 'Typography', 'ectbe' ),
					'selector' 	=> '{{WRAPPER}} .ectbe-tribe-events-back ',
				]
            );

        	$this->add_control(
			'ectbe_allevent_color', //param_name
			[
				'label' 		=> __( 'Color', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-tribe-events-back a' => 'color: {{VALUE}}',
				],
			]
        );
        		$this->add_control(
			'ectbe_allevent_bg_color', //param_name
			[
				'label' 		=> __( 'Background Color', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-tribe-events-back ' => 'background: {{VALUE}}',
				],
			]
        );
     

        $this->add_control(
            'ectbe_link_text',
            [
                'label'       => __('Text', 'ectbe'),
                'type' => \Elementor\Controls_Manager::TEXT,             
                'default'     => __('All Events','the-event-calendar'),
                
            ]
        );
        $this->add_control(
			'ectbe_custom_enable_url',
			[
				'label' => __( 'Enable Custom Link', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
        );
        $this->add_control(
			'ectbe_custom_url_link',
			[
				'label' => __( 'Custom Link', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::URL,
				'placeholder' => __( 'https://your-link.com', 'plugin-domain' ),
                'show_external' => true,
                  'condition' => [
                         'ectbe_custom_enable_url' => 'yes'
                    ],
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

        $this->end_controls_section();

    }

    protected function render()
    {
        
        $settings = $this->get_settings_for_display();       
        $text = $settings['ectbe_link_text'];
        $custom_url=isset($settings['ectbe_custom_url_link'])?$settings['ectbe_custom_url_link']:'';
     
        $url=($settings['ectbe_custom_enable_url']=="yes")?$custom_url['url']:tribe_get_events_link();

        $all_events = '<div class="ectbe-evt-all-link">';
       
        $all_events .= '<p class="ectbe-tribe-events-back">
                <a href="' . $url . '"target="_blank">' . $text . '</a>
                    </p>';
        $all_events .= "</div>";

        echo $all_events;
    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_all_events());
