<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow; 

class Ectbe_widgets_tickets extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }

    public function get_style_depends() {
        $style=[];
        if (\Elementor\Plugin::$instance->editor->is_edit_mode() || \Elementor\Plugin::$instance->preview->is_preview_mode()) {
            return [ tribe_asset_enqueue('tribe-tickets-rsvp-style','tribe-tickets-form-style','tribe-tickets-rsvp','tribe-common-responsive')];
        }
        return $style;
    }
    public function get_name()
    {
        return "the-event-tickets";
    }

    public function get_title()
    {
        return "Event Tickets";
    }

    public function get_icon()
    {
        return 'fa fa-ticket';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_ticket_section',
            [
                'label' => __('Event Tickets', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
                $this->add_control(
			'ectbe_content_title_heading',
			[
				'label' => __( 'Title ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
            $this->add_control(
            'ectbe_ticket_title_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} #tribe-tickets h2' => 'color: {{VALUE}}',
                ],
            ]
        );
        
              $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_ticket_typography',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} #tribe-tickets h2',
            ]
        );
           $this->add_control(
			'ectbe_content_heading',
			[
				'label' => __( 'Content ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
     
          $this->add_control(
            'ectbe_ticket_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} #tribe-tickets ,{{WRAPPER}} .tribe-tickets__item div' => 'color: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_ticket_bg_color',
            [
                'label' 		=> __( 'Background Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} #tribe-tickets,{{WRAPPER}} .tribe-tickets__item' => 'background: {{VALUE}}',
                ],
            ]
        );
 
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_ticket_text_typography',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>  '{{WRAPPER}} #tribe-tickets',
            ]
        );
    

               $this->add_control(
			'ectbe_content_btn_heading',
			[
				'label' => __( 'Button ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
               $this->add_control(
            'ectbe_ticket_btn_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-common-c-btn' => 'color: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_ticket_btn_bg_color',
            [
                'label' 		=> __( 'Background Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-common-c-btn' => 'background: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_ticket_btn_bg_hover_color',
            [
                'label' 		=> __( 'Background Hover Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-common-c-btn:hover ' => 'background: {{VALUE}}',
                ],
            ]
        );       
     
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_ticket_button_typography',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>  '{{WRAPPER}} .tribe-common-c-btn',
            ]
        );     

        $this->end_controls_section();

    }

    protected function render()
    {
        $single_page_meta_event_map_op = '';
        $id = get_the_ID();
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
            if(empty(Tribe__Tickets__Tickets_View::instance()->get_tickets_block($id,false))){                
            echo'<span class="ectbe-alert-msg"><b>'. __('This widget is displayed if Tickets is set in this page.', 'ectbe').'</b></span>';
            }
        }

         if (class_exists('Tribe__Tickets__Main')) {           
        
         echo Tribe__Tickets__Tickets_View::instance()->get_tickets_block($id,false);
            
        }            
      
    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_tickets());
