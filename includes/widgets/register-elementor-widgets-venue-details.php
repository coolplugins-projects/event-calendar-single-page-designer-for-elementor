<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
 use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow; 
use Elementor\Group_Control_Background ;
 



class Ectbe_widgets_venue_details extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }

    public function get_name()
    {
        return "the-event-venue-details";
    }

    public function get_title()
    {
        return "Event Venue Details";
    }

    public function get_icon()
    {
        return 'fas fa-map';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'venue_content_section',
            [
                'label' => __('Event Venue Contents', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
			'ectbe_enable_custom_txt',
			[
				'label' => __( 'Enable Custom Labels', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
		);
        $this->add_control(
			'ectbe_venue_widget_title',
			[
				'label' => __( 'Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Venue', 'ectbe' ),
                'placeholder' => __( 'Type your title here', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_txt' => 'yes'],
			]
        );
          $this->add_control(
			'ectbe_venue_widget_address',
			[
				'label' => __( 'Address label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Address', 'ectbe' ),
                'placeholder' => __( 'Type your title here', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_txt' => 'yes'],
			]
        );
    
          $this->add_control(
			'ectbe_venue_widget_phone',
			[
				'label' => __( 'Phone Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Phone:', 'ectbe' ),
                'placeholder' => __( 'Type your Phone number', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_txt' => 'yes'],
			]
        );
           $this->add_control(
			'ectbe_venue_website_label',
			[
				'label' => __( 'Website Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Website:', 'ectbe' ),
                'placeholder' => __( 'Type your website label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_txt' => 'yes'],
			]
        );


        $this->add_control(
			'view_hidden_ff',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'traditional',
                 'selectors' 	=> [
                    '
                    
                    {{WRAPPER}} .ectbe-event-venue-details h2,
                    {{WRAPPER}} .ectbe-events-event-meta h2' => 'width: 100%',
                ],
			]
        );
             
       	$this->add_control(
			'ectbe_venue_btn_heading',
			[
				'label' => __( 'Title ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);


            $this->add_control(
            'ectbe_venue_title_color',
            [
                'label' 		=> __( 'Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '
                    
                    {{WRAPPER}} .ectbe-event-venue-details h2,
                    {{WRAPPER}} .ectbe-events-event-meta h2' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this->add_control(
            'ectbe_venue_title_bg_color',
            [
                'label' 		=> __( 'Background Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '
                    
                    {{WRAPPER}} .ectbe-event-venue-details h2,
                    {{WRAPPER}} .ectbe-events-event-meta h2' => 'background: {{VALUE}}',
                ],
            ]
        );
            $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_venue_typography_org',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} .ectbe-event-venue-details h2,
                                     {{WRAPPER}} .ectbe-events-event-meta h2',
            ]
        );
         	$this->add_control(
			'ectbe_venue_btn_heading_content',
			[
				'label' => __( 'Content ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
      
        $this->add_control(
            'ectbe_venue_first_color',
            [
                'label' 		=> __( 'Label Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .ectbe-event-venue-details dt,
                      
                  
                    
                 
                    {{WRAPPER}} .ectbe-events-event-meta span b' => 'color: {{VALUE}}',
                ],
            ]
        );
          $this->add_control(
            'ectbe_venue_second_color',
            [
                'label' 		=> __( 'Content Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '
                      {{WRAPPER}}  .ectbe-events-venue-details ,
                      {{WRAPPER}}  .ectbe-events-venue-details a,
                    {{WRAPPER}} .ectbe-event-venue-details dd,
                    {{WRAPPER}} .ectbe-event-venue-details dd a,
                    
                    
                    {{WRAPPER}} .ectbe-events-event-meta span,
                    {{WRAPPER}} .ectbe-events-event-meta span a' => 'color: {{VALUE}}',
                ],
            ]
        );

       $this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'ectbe_meta_background',
				'label' => __( 'Background', 'ectbe' ),
				'types' => [ 'classic', 'gradient'],
                'selector' => '{{WRAPPER}} .ectbe-event-venue-details,
                {{WRAPPER}}  .ectbe-events-venue-details ,
                {{WRAPPER}} .ectbe-events-event-meta ',
			]
		);

            
     
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_venue_typography_title',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} .ectbe-event-venue-details dt,
                    {{WRAPPER}} .ectbe-event-venue-details dd,
                     {{WRAPPER}}  .ectbe-events-venue-details ,
                      {{WRAPPER}}  .ectbe-events-venue-details a,
                      {{WRAPPER}} .ectbe-events-event-meta span',
            ]
        );

    

        $this->add_responsive_control(
            'ectbe_venue_padding', //param_name
            [
                'label' 		=> __( 'Padding', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} .ectbe-event-venue-details dt,
                    {{WRAPPER}} .ectbe-event-venue-details dd,
                    {{WRAPPER}} .ectbe-event-venue-details dd a,
                    {{WRAPPER}} .ectbe-event-venue-details h2,
                    {{WRAPPER}}  .ectbe-events-venue-details ,
                      {{WRAPPER}}  .ectbe-events-venue-details a,
                      {{WRAPPER}} .ectbe-events-event-meta span' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_venue_margin', //param_name
            [
                'label' 		=> __( 'Margin', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} .ectbe-event-venue-details dt,
                    {{WRAPPER}} .ectbe-event-venue-details dd,
                    {{WRAPPER}} .ectbe-event-venue-details dd a,
                    {{WRAPPER}} .ectbe-event-venue-details h2,
                    {{WRAPPER}}  .ectbe-events-venue-details,
                      {{WRAPPER}}  .ectbe-events-venue-details a,
                      {{WRAPPER}} .ectbe-events-event-meta span' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        	$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' 		=> 'ectbe_venue_box_shadow',
				'label' 	=> __('Box Shadow', 'ectbe'),
                'selector' 	=> '{{WRAPPER}} .ectbe-event-venue-details,
                                {{WRAPPER}}  .ectbe-events-event-meta',
			]
		);

        $this->end_controls_section();


    }

    /**
     * Get meta venue template
     */
    private function tec_get_meta_venue_template()
    {
        ob_start();
        if (function_exists('tribe_get_template_part')) {
            tribe_get_template_part('modules/meta/venue');
        }
        return ob_get_clean();
    }

    protected function render()
    {
        $id = get_the_ID();

        $settings = $this->get_settings_for_display();
        $title = !empty($settings['ectbe_venue_widget_title']) ? $settings['ectbe_venue_widget_title'] : '';
        
        $address=!empty($settings['ectbe_venue_widget_address']) ? $settings['ectbe_venue_widget_address'] : '';

       
        $phone=!empty($settings['ectbe_venue_widget_phone'])?$settings['ectbe_venue_widget_phone']:'';
      
        $wesite_label = !empty($settings['ectbe_venue_website_label']) ? $settings['ectbe_venue_website_label'] : '';


        if($settings['ectbe_enable_custom_txt']=="yes"){

              if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
        $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
              }
        
       
   
    // Setup an array of venue details for use later in the template
    $venue_details = tribe_get_venue_details($id);
    // Venue
    $has_venue_address = (!empty($venue_details['address'])) ? ' location' : '';
   $check_phn=!empty(tribe_get_phone($id)) ?$phone : '';
   $chek_email=!empty(tribe_get_venue_website_link($id)) ?$wesite_label : '';

        //    do_action('tribe_events_before_the_meta');            
           ?>
    <div class="ectbe-events-event-meta">
            <h2 class="ectbe-events-single-section-title"> <?php echo$title; ?></h2>
    	<div class="author <?php echo esc_attr($has_venue_address); ?>">    		

    		<?php if ($venue_details): ?>
    			<!-- Venue Display Info -->
    			<div class="ectbe-events-venue-details">
                    <span><b><?php echo !empty( $venue_details) ?$address : ''; ?></b><br></span>
    				<?php echo implode('<br> ', $venue_details); ?>
    				<?php
    if (tribe_show_google_map_link()) {
        echo tribe_get_map_link_html();
    }

     echo $phone_email='<br><span><b>'.$check_phn.'</b> <br>'.tribe_get_phone($id).'</span><br><span><b>'.$chek_email.'</b>
    <br>'.tribe_get_venue_website_link($id).'</span>';
 
    ?>
    			</div> <!-- .ectbe-events-venue-details -->
    		<?php endif;?>

    	</div>
    </div><!-- .ectbe-events-event-meta -->
    <?php


        }
        else{


        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
              if(empty(tribe_get_phone($id)||tribe_get_venue_website_link($id))){
                
                echo'<span class="ectbe-alert-msg"><b>'. __('This widget is displayed if Venue is set in this page.', 'ectbe').'</b></span>';


            }
       if(!empty(tribe_get_phone($id)||tribe_get_venue_website_link($id))){
             $check_dphn=!empty(tribe_get_phone($id)) ?'Phone:': '';
   $chek_demail=!empty(tribe_get_venue_website_link($id)) ?'Website:': '';
    // Setup an array of venue details for use later in the template
    $venue_details = tribe_get_venue_details($id);

    // Venue
    $has_venue_address = (!empty($venue_details['address'])) ? ' location' : '';
    
            $vtitlee=!empty(tribe_get_phone($id)||tribe_get_venue_website_link($id))?'Venue':'';
           
          // do_action('tribe_events_before_the_meta');            
           ?>
    <div class="ectbe-events-event-meta">
            <h2 class="ectbe-events-single-section-title"> <?php echo$vtitlee; ?> </h2>
    	<div class="author <?php echo esc_attr($has_venue_address); ?>">    		

    		<?php if ($venue_details): ?>
    			<!-- Venue Display Info -->
    			<div class="ectbe-events-venue-details">
    				<?php echo implode('<br> ', $venue_details); ?>
    				<?php
    if (tribe_show_google_map_link()) {
        echo tribe_get_map_link_html();
    }

     echo $phone_email='<br><span><b>'.$check_dphn.'</b> <br>'.tribe_get_phone($id).'</span><br><span><b>'.$chek_demail.'</b>
    <br>'.tribe_get_venue_website_link($id).'</span>';
 
    ?>
    			</div> <!-- .ectbe-events-venue-details -->
    		<?php endif;?>

    	</div>
    </div><!-- .ectbe-events-event-meta -->
    <?php 
       }

            }
    else{
    
    $css_class = 'ectbe-event-venue-details';

    $tribe_single_page_venue_op = '';
    if (!empty($css_class)) {
    $tribe_single_page_venue_op .= '<div class="' . esc_attr($css_class) . '">';
    }
    // Tribe__Events__Main::instance()->VenueMetaBox()
   if(!empty(tribe_get_phone($id)||tribe_get_venue_website_link($id))){
    $tribe_single_page_venue_op .= $this->tec_get_meta_venue_template();
   }

    if (!empty($css_class)) {
    $tribe_single_page_venue_op .= '</div>';
    }
    echo $tribe_single_page_venue_op; 

    }
}

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_venue_details());
