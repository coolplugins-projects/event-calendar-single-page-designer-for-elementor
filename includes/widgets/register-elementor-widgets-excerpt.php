<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;

use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

class Ectbe_widgets_excerpt extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

     
    }
    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
 
    public function get_name()
    {
        return "the-event-excerpt";
    }

    public function get_title()
    {
        return "Event Excerpt";
    }

    public function get_icon()
    {
        return 'eicon-post-excerpt';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'excerpt_section',
            [
                'label' => __('Event Exerpt', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' 		=> 'excerpt_typography',
					'label' 	=> __( 'Typography', 'ectbe' ),
					'selector' 	=> '{{WRAPPER}} .ectbe-evt-excerpt',
				]
			);

			$this->add_control(
				'ectbe_excerpt',
				[
					'label' 		=> __( 'Excerpt Color', 'color' ),
					'type' 			=> \Elementor\Controls_Manager::COLOR, 
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-evt-excerpt, {{WRAPPER}} .ectbe-evt-excerpt p' => 'color: {{VALUE}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'ectbe_excerpt_padding', //param_name
				[
					'label' 		=> __( 'Padding', 'ectbe' ), //heading
					'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
					'size_units' 	=> [ 'px', 'em', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-evt-excerpt' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'ectbe_excerpt_margin', //param_name
				[
					'label' 		=> __( 'Margin', 'ectbe' ), //heading
					'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
					'size_units' 	=> [ 'px', 'em', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-evt-excerpt' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' 			=> 'ectbe_excerpt_border',
					'label' 		=> __( 'Border', 'ectbe' ),
					'selector' 		=> '{{WRAPPER}} .ectbe-evt-excerpt',
				]
			);

			$this->add_control(
				'ectbe_title_shape_radius', //param_name
				[
					'label' 		=> __( 'Border Radius', 'ectbe' ), //heading
					'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
					'size_units' 	=> [ 'px', 'em', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-evt-excerpt' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);
	
			$this->add_group_control(
				Group_Control_Box_Shadow::get_type(),
				[
					'name' 		=> 'ectbe_title_box_shadow',
					'label' 	=> __('Box Shadow', 'ectbe'),
					'selector' 	=> '{{WRAPPER}} .ectbe-evt-excerpt',
				]
			);   

        $this->end_controls_section();

    }

    protected function render()
    {
        $id = get_the_ID();
        if ( Elementor\Plugin::$instance->editor->is_edit_mode() ) { 
            $post = get_posts( 'post_type=tribe_events&numberposts=1' );
            $post = $post[0];
            $id = $post->ID;
        }       

        if( has_excerpt($id) ){      
            $event_excerpt ='';
               $get_event_excerpt = get_the_excerpt($id);              
               echo  $event_excerpt .= '<div class="ectbe-evt-excerpt">'.$get_event_excerpt.'</div>';                           
            
            }
    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_excerpt());
