<?php
use Elementor\Controls_Manager;
use Elementor\Group_Control_Background;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

class Ectbe_widgets_meta_details extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }

    public function get_name()
    {
        return "the-event-meta-details";
    }

    public function get_title()
    {
        return "Event Meta Details";
    }

    public function get_icon()
    {
        return 'fas fa-receipt';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'ectbe_meta_detail_content_section',
            [
                'label' => __('Event Meta Details', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

         $this->add_control(
			'ectbe_enable_custom_meta_txt',
			[
				'label' => __( 'Enable Custom Labels', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
        );
          $this->add_control(
			'ectbe_meta_custom_title',
			[
				'label' => __( 'Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Details', 'ectbe' ),
                'placeholder' => __( 'Type your title here', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_meta_txt' => 'yes'],
			]
        );
        $this->add_control(
			'ectbe_meta_custom_date',
			[
				'label' => __( 'Date Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Date', 'ectbe' ),
                'placeholder' => __( 'Type your date here', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_meta_txt' => 'yes'],
			]
        );
          
		
        $this->add_control(
			'ectbe_meta_custom_time',
			[
				'label' => __( 'Time Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Time:', 'ectbe' ),
                'placeholder' => __( 'Type your time label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_meta_txt' => 'yes'],
			]
        );
          $this->add_control(
			'ectbe_meta_custom_cost',
			[
				'label' => __( 'Cost Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Cost:', 'ectbe' ),
                'placeholder' => __( 'Type your Cost Label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_meta_txt' => 'yes'],
			]
        );
           $this->add_control(
			'ectbe_meta_custom_catagory',
			[
				'label' => __( 'Category Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Category:', 'ectbe' ),
                'placeholder' => __( 'Type your Category label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_meta_txt' => 'yes'],
			]
        );
           $this->add_control(
			'ectbe_meta_custom_tag',
			[
				'label' => __( 'Tag Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Tag:', 'ectbe' ),
                'placeholder' => __( 'Type your Category label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_meta_txt' => 'yes'],
			]
        );
          $this->add_control(
			'ectbe_meta_cwebsite_label',
			[
				'label' => __( 'Website Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Website:', 'ectbe' ),
                'placeholder' => __( 'Type your website label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_meta_txt' => 'yes'],
			]
        );
              	$this->add_control(
			'ectbe_meta_btn_heading',
			[
				'label' => __( 'Title ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);    
     
   
        $this->add_control(
            'ectbe_meta_TITLE_first_color',
            [
                'label' => __('Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details h2' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this->add_control(
            'ectbe_meta_TITLE_bg_first_color',
            [
                'label' => __('Background Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details h2' => 'background: {{VALUE}}',
                ],
            ]
        );
           $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ectbe_meta_detail_typography_org',
                'label' => __('Typography', 'ectbe'),
                'selector' => '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details h2',
            ]
        );
             	$this->add_control(
			'ectbe_meta_btn_head_content',
			[
				'label' => __( 'Content ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

        $this->add_control(
            'ectbe_meta_detail_first_color',
            [
                'label' => __('Label Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dt


                    ' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'ectbe_second_detail_title_color',
            [
                'label' => __('Content Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dd,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dd a' => 'color: {{VALUE}}',
                ],
            ]
        );
             $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ectbe_meta_detail_typography_title',
                'label' => __('Typography', 'ectbe'),
                'selector' => '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dt,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dd',
            ]
        );
        $this->add_group_control(
            \Elementor\Group_Control_Background::get_type(),
            [
                'name' => 'ectbe_meta_background',
                'label' => __('Background', 'ectbe'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details',
            ]
        );

        $this->add_control(
            'ectbe_meta_detail_hidden_padding',
            [
                'label' => __('View', 'plugin-domain'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'traditional',
                'selectors' => [
                    '
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details h2' => 'width:100%',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_meta_detail_padding', //param_name
            [
                'label' => __('Title Padding', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dt,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dd,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dd a,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details h2' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_meta_detail_margin', //param_name
            [
                'label' => __('Title Padding', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dt,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dd,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details dd a,
                    {{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details h2' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'ectbe_meta_detail_box_shadow',
                'label' => __('Box Shadow', 'ectbe'),
                'selector' => '{{WRAPPER}} .ectbe-events-meta-group.ectbe-events-meta-group-details',
            ]
        );

        $this->end_controls_section();

    }



    protected function render()
    {
            $settings = $this->get_settings_for_display();

$custom_title = !empty($settings['ectbe_meta_custom_title']) ? $settings['ectbe_meta_custom_title'] : 'Details';

        $date_lbl= !empty($settings['ectbe_meta_custom_date']) ? $settings['ectbe_meta_custom_date'] : 'Date';

$time_lbl = !empty($settings['ectbe_meta_custom_time']) ? $settings['ectbe_meta_custom_time'] : 'Time:';
$cost_lbl = !empty($settings['ectbe_meta_custom_cost']) ? $settings['ectbe_meta_custom_cost'] : 'Cost:';
$category_lbl = !empty($settings['ectbe_meta_custom_catagory']) ? $settings['ectbe_meta_custom_catagory'] : 'Event Category';
$tag_lbl = !empty($settings['ectbe_meta_custom_tag']) ? $settings['ectbe_meta_custom_tag'] : 'Event Tags:';

$wesite_label = !empty($settings['ectbe_meta_cwebsite_label']) ? $settings['ectbe_meta_cwebsite_label'] : 'Website:';

        $id = get_the_ID();
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
        }

            $event_id = Tribe__Main::post_id_helper($id);
            $time_format = get_option('time_format', Tribe__Date_Utils::TIMEFORMAT);
            $time_range_separator = tribe_get_option('timeRangeSeparator', ' - ');
            $show_time_zone = tribe_get_option('tribe_events_timezones_show_zone', false);
            $time_zone_label = Tribe__Events__Timezones::get_event_timezone_abbr($event_id);

            $start_datetime = tribe_get_start_date($id);
            $start_date = tribe_get_start_date(null, false);
            $start_time = tribe_get_start_date(null, false, $time_format);
            $start_ts = tribe_get_start_date(null, false, Tribe__Date_Utils::DBDATEFORMAT);

            $end_datetime = tribe_get_end_date($id);
            $end_date = tribe_get_display_end_date(null, false);
            $end_time = tribe_get_end_date(null, false, $time_format);
            $end_ts = tribe_get_end_date(null, false, Tribe__Date_Utils::DBDATEFORMAT);

            $time_formatted = null;
            if ($start_time == $end_time) {
                $time_formatted = esc_html($start_time);
            } else {
                $time_formatted = esc_html($start_time . $time_range_separator . $end_time);
            }

/**
 * Returns a formatted time for a single event
 *
 * @var string Formatted time string
 * @var int Event post id
 */
 //   $time_formatted = apply_filters('tribe_events_single_event_time_formatted', $time_formatted, $event_id);

/**
 * Returns the title of the "Time" section of event details
 *
 * @var string Time title
 * @var int Event post id
 */
          //  $time_title = apply_filters('tribe_events_single_event_time_title', __($time_lbl, 'the-events-calendar'), $event_id);

            $cost = tribe_get_formatted_cost($id);
            $website = tribe_get_event_website_link($id);
            ?>

<div class="ectbe-events-meta-group ectbe-events-meta-group-details">
	<h2 class="ectbe-events-single-section-title"> <?php esc_html_e($custom_title, 'the-events-calendar');?> </h2>
	<dl>
		<?php
//do_action('tribe_events_single_meta_details_section_start');
            // All day (multiday) events
            if (tribe_event_is_all_day() && tribe_event_is_multiday()):
            ?>
			<dt class="ectbe-events-start-date-label"> <?php esc_html_e('Start:', 'the-events-calendar');?> </dt>
			<dd>
				<abbr class="ectbe-events-abbr ectbe-events-start-date published dtstart" title="<?php echo esc_attr($start_ts); ?>"> <?php echo esc_html($start_date); ?> </abbr>
			</dd>
			<dt class="ectbe-events-end-date-label"> <?php esc_html_e('End:', 'the-events-calendar');?> </dt>
			<dd>
				<abbr class="ectbe-events-abbr ectbe-events-end-date dtend" title="<?php echo esc_attr($end_ts); ?>"> <?php echo esc_html($end_date); ?> </abbr>
			</dd>

		<?php
// All day (single day) events
            elseif (tribe_event_is_all_day()):
            ?>

			<dt class="ectbe-events-start-date-label"> <?php esc_html_e($date_lbl, 'the-events-calendar');?> </dt>
			<dd>
				<abbr class="ectbe-events-abbr ectbe-events-start-date published dtstart" title="<?php echo esc_attr($start_ts); ?>"> <?php echo esc_html($start_date); ?> </abbr>
			</dd>

		<?php
// Multiday events
            elseif (tribe_event_is_multiday()):
            ?>

			<dt class="ectbe-events-start-datetime-label"> <?php esc_html_e('Start:', 'the-events-calendar');?> </dt>
			<dd>
				<abbr class="ectbe-events-abbr ectbe-events-start-datetime updated published dtstart" title="<?php echo esc_attr($start_ts); ?>"> <?php echo esc_html($start_datetime); ?> </abbr>
				<?php if ($show_time_zone): ?>
					<span class="ectbe-events-abbr ectbe-events-time-zone published "><?php echo esc_html($time_zone_label); ?></span>
				<?php endif;?>
			</dd>

			<dt class="ectbe-events-end-datetime-label"> <?php esc_html_e('End:', 'the-events-calendar');?> </dt>
			<dd>
				<abbr class="ectbe-events-abbr ectbe-events-end-datetime dtend" title="<?php echo esc_attr($end_ts); ?>"> <?php echo esc_html($end_datetime); ?> </abbr>
				<?php if ($show_time_zone): ?>
					<span class="ectbe-events-abbr ectbe-events-time-zone published "><?php echo esc_html($time_zone_label); ?></span>
				<?php endif;?>
			</dd>

		<?php
// Single day events
            else:
            ?>

			<dt class="ectbe-events-start-date-label"> <?php esc_html_e($date_lbl, 'the-events-calendar');?> </dt>
			<dd>
				<abbr class="ectbe-events-abbr ectbe-events-start-date published dtstart" title="<?php echo esc_attr($start_ts); ?>"> <?php echo esc_html($start_date); ?> </abbr>
			</dd>

			<dt class="ectbe-events-start-time-label"> <?php echo esc_html($time_lbl); ?> </dt>
			<dd>
				<div class="ectbe-events-abbr ectbe-events-start-time published dtstart" title="<?php echo esc_attr($end_ts); ?>">
					<?php echo $time_formatted; ?>
					<?php if ($show_time_zone): ?>
						<span class="ectbe-events-abbr ectbe-events-time-zone published "><?php echo esc_html($time_zone_label); ?></span>
					<?php endif;?>
				</div>
			</dd>

		<?php endif?>

		<?php
// Event Cost
            if (!empty($cost)): ?>

			<dt class="ectbe-events-event-cost-label"> <?php esc_html_e($cost_lbl, 'the-events-calendar');?> </dt>
			<dd class="ectbe-events-event-cost"> <?php echo esc_html($cost); ?> </dd>
		<?php endif?>

		<?php
echo tribe_get_event_categories(
                $id,
                [
                    'before' => '',
                    'sep' => ', ',
                    'after' => '',
                    'label' => $category_lbl, // An appropriate plural/singular label will be provided
                    'label_before' => '<dt class="ectbe-events-event-categories-label">',
                    'label_after' => '</dt>',
                    'wrap_before' => '<dd class="ectbe-events-event-categories">',
                    'wrap_after' => '</dd>',
                ]
            );
            ?>

		<?php
/* Translators: %s: Event (singular) */
            tribe_meta_event_tags(sprintf(esc_html__('%s'.$tag_lbl, 'the-events-calendar'), tribe_get_event_label_singular($id)), ', ', true);
            ?>

		<?php
// Event Website
            if (!empty($website)): ?>

			<dt class="ectbe-events-event-url-label"> <?php esc_html_e($wesite_label, 'the-events-calendar');?> </dt>
			<dd class="ectbe-events-event-url"> <?php echo $website; ?> </dd>
		<?php endif?>

	
	</dl>
</div>
<?php

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_meta_details());
