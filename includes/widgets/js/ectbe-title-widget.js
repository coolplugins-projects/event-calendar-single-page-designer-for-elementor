class CoolEventsWidgets extends elementorModules.frontend.handlers.Base {
    getDefaultSettings() {
        return {
            selectors: {
                title: '.cool-event-title',
                content: '.contentClassName',
            },
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings('selectors');
        return {
            title: this.$element.find(selectors.title),
        };
    }

    bindEvents() {
        return
        var title = this.elements.title;
        let headingTag = title.attr('data-heading-tag');
        let fontSize = title.attr('data-fontSize');
        jQuery(title).html('<' + headingTag + ' style="font-size:' + fontSize + ';">Dummy Title</' + headingTag + '>');
        console.log(title);
    }
}

jQuery(window).on('elementor/frontend/init', () => {
    const addHandler = ($element) => {

        elementorFrontend.elementsHandler.addHandler(CoolEventsWidgets, {
            $element,
        });
    };
    elementorFrontend.hooks.addAction('frontend/element_ready/the-event-title.default', addHandler);
});