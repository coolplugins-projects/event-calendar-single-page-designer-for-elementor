class ectbe_reg_form extends elementorModules.frontend.handlers.Base {
    getDefaultSettings() {
        return {
            selectors: {
                swiperContainer: '#ectbe_reg_form',
                btn_hide: '#ectbe_reg_custom .rtec-outer-wrap',
            },
        };
    }

    getDefaultElements() {
        const selectors = this.getSettings('selectors');
        return {
            $swiperContainer: this.$element.find(selectors.swiperContainer),
            $btn_hide: this.$element.find(selectors.btn_hide),
        };
    }

    bindEvents() {
        var selector = this.elements.$swiperContainer,
            btn = this.elements.$btn_hide,
            $ = jQuery;
            $('#rtec h3').hide();
            $(btn).hide();
            $(selector).click(function (e) {
                $(btn).toggle('slow');
            
            })
    }

}

jQuery(window).on('elementor/frontend/init', () => {

    const addHandler = ($element) => {
        elementorFrontend.elementsHandler.addHandler(ectbe_reg_form, {
            $element,
        });
    };

    elementorFrontend.hooks.addAction('frontend/element_ready/the-event-registration-form.default', addHandler);

});