<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

class Ectbe_widgets_google_cal_export extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);       
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
   

    public function get_name()
    {
        return "the-event-google-cal-export";
    }

    public function get_title()
    {
        return "Google Calendar & iCal Export";
    }

    public function get_icon()
    {
        return 'fas fa-file-export';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
			'export_section',
			[
				'label' => __( 'Google & Ical Text', 'ectbe' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

		$this->add_control(
			'ectbe_google_title',
			[
				'label' => __( 'Google Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '+ Google Calendar', 'ectbe' ),
				
			]
        );
        $this->add_control(
			'ectbe_icall_title',
			[
				'label' => __( 'Ical Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( '+ ICal Export', 'ectbe' ),
				
			]
		);

			   $this->add_control(
			'ectbe_content_btn_heading',
			[
				'label' => __( 'Button ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
		$this->add_control(
			'ectbe_export_button',
			[
				'label' 		=> __( 'Color', 'ectbe' ),
				'type' 			=> \Elementor\Controls_Manager::COLOR, 
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button a,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button a' => 'color: {{VALUE}}',
				],
			]
		);	


		$this->add_control(
			'ectbe_export_button_bg',
			[
				'label' 		=> __( 'Background Color', 'color' ),
				'type' 			=> \Elementor\Controls_Manager::COLOR, 
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button ,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button ' => 'background: {{VALUE}}',
				],
			]
		);		

		$this->add_control(
			'ectbe_export_button_bg_hover',
			[
				'label' 		=> __( 'Hover Background Color', 'color' ),
				'type' 			=> \Elementor\Controls_Manager::COLOR, 
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button:hover,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button:hover' => 'background: {{VALUE}}',
				],
			]
		);		
				$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'title_typography',
				'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=> '{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button ,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button ',
			]
		);
				$this->add_control(
			'ectbe_featured_btn_border_radius',
			[
				'label' 		=> __( 'Border Radius', 'ectbe' ),
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button ,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button ' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' 			=> 'ectbe_btn_box_border',
				'label' 		=> __( 'Border', 'ectbe' ),
				'selector' 		=> '{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button ,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button ',
			]
		);
		
		$this->add_responsive_control(
			'ectbe_export_button_padding', //param_name
			[
				'label' 		=> __( 'Padding', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button ,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button ' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'ectbe_export_button_margin', //param_name
			[
				'label' 		=> __( 'Margin', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links span.ectbe-gcal-button ,
                                {{WRAPPER}} .ectbe-events-links span.ectbe-ical-button ' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		
		$this->add_control(
			'ectbe_display',
			[
				'label' => __( 'Display', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::SELECT,
				'default' => 'block',
				'options' => [
					'table-cell'  => __( 'table-cell', 'ectbe' ),
					'block' => __( 'block', 'ectbe' ),
					'inline-block' => __( 'inline-block', 'ectbe' ),
				],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links' => 'display: {{VALUE}}',
				],
			]
		);
	

		$this->add_control(
			'ectbe_text_align',
			[
				'label' => __( 'Alignment', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'ectbe' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'ectbe' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'ectbe' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'center',
				'toggle' => true,
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links' => 'text-align: {{VALUE}}',
				],
			]
		);
	   $this->add_control(
			'ectbe_container_heading',
			[
				'label' => __( 'Container settings ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
	

		$this->add_control(
			'ectbe_export_box_bg_color', //param_name
			[
				'label' 		=> __('Background Color', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links' => 'background: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'ectbe_export_box_padding', //param_name
			[
				'label' 		=> __('Padding', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'ectbe_export_box_margin', //param_name
			[
				'label' 		=> __('Margin', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' 			=> 'ectbe_export_box_border',
				'label' 		=> __('Border', 'ectbe'),
				'selector' 		=> '{{WRAPPER}} .ectbe-events-links',
			]
		);

		$this->add_control(
			'ectbe_export_box_shape_radius', //param_name
			[
				'label' 		=> __('Border Radius', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-links' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' 		=> 'ectbe_export_box_box_shadow',
				'label' 	=> __('Box Shadow', 'ectbe'),
				'selector' 	=> '{{WRAPPER}} .ectbe-events-links',
			]
		);		

		$this->end_controls_section();	

    }

    protected function render()
    {
        $id = get_the_ID();
        if(Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
        }
        $settings = $this->get_settings_for_display();
        $google =!empty($settings['ectbe_google_title'])?$settings['ectbe_google_title']:'';
        $ical = !empty($settings['ectbe_icall_title'])?$settings['ectbe_icall_title']:'';


        $single_page_cal_link_op = '';
        $single_page_gcal_link ='';
        $single_page_cal_link ='';
        if(function_exists('tribe_get_single_ical_link')){
		$single_page_cal_link = esc_url( tribe_get_single_ical_link());
		echo'<style>
		 .ectbe-events-links span.ectbe-gcal-button {width:100%;}
                             .ectbe-events-links span.ectbe-ical-button {width:100%;} </style>';
        }
        if ( 'tribe_events' == get_post_type() ){
         $single_page_gcal_link = Tribe__Events__Main::instance()->esc_gcal_url( tribe_get_gcal_link());
         }
        // don't show on password protected posts
         if ( is_single() && post_password_required() ) {
            return;
         }
      
      
        $single_page_cal_link_op .= '<div class="ectbe-events-links">';
         $single_page_cal_link_op .= '<span class="ectbe-gcal-button"><a  href="' .$single_page_gcal_link. '" title="' . $google . '">' .$google . '</a></span>';
         $single_page_cal_link_op .= '<span class="ectbe-ical-button"><a  href="' .$single_page_cal_link. '" title="' . esc_attr__( 'Download .ics file', 'the-events-calendar' ) . '" >' .$ical . '</a></span>';
        $single_page_cal_link_op .= '</div>';
      
        echo $single_page_cal_link_op;
    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_google_cal_export());
