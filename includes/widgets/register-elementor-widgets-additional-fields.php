<?php

use Elementor\Controls_Manager;
use Elementor\Widget_Base;
 use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow; 
use Elementor\Group_Control_Background ;

class Ectbe_widgets_additional_fields extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        //  wp_register_script( 'cool-events-widget', plugin_dir_url(__FILE__) .'js/cool-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['cool_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['cool-events-widget'];
    }

    public function get_name()
    {
        return "the-event-additional-fields";
    }

    public function get_title()
    {
        return "Event Additional Fields";
    }

    public function get_icon()
    {
        return 'fab fa-buromobelexperte';
    }

    // register controls
    protected function _register_controls()
    {
  $this->start_controls_section(
            'ect_additional_section',
            [
                'label' => __('Event Additional Section', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
			'view_additional_ff',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'traditional',
                 'selectors' 	=> ['                    
                    {{WRAPPER}} .ectbe-meta-additional-field h2,
                    {{WRAPPER}} .tribe-events-event-meta h2' => 'width: 100%',
                ],
			]
        );
          	$this->add_control(
			'ectbe_content__passed_section',
			[
				'label' => __( 'Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
            $this->add_control(
			'ectbe_additional_custom_title',
			[
				'label' => __( 'Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Other', 'ectbe' ),
                'placeholder' => __( 'Type your title here', 'ectbe' ),
               
			]
        );
         
        $this->add_control(
            'ectbe_additional_title_color',
            [
                'label' 		=> __( 'Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '
                    
                    {{WRAPPER}} .ectbe-meta-additional-field h2,
                    {{WRAPPER}} .tribe-events-event-meta h2' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this->add_control(
            'ectbe_additional_title_bg_color',
            [
                'label' 		=> __( 'Background Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '
                    
                    {{WRAPPER}} .ectbe-meta-additional-field h2,
                    {{WRAPPER}} .tribe-events-event-meta h2' => 'background: {{VALUE}}',
                ],
            ]
        );
           $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_additional_typography_org',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} .ectbe-meta-additional-field h2
                                     ',
            ]
        );

          	$this->add_control(
			'ectbe_content_passed_section',
			[
				'label' => __( 'Content', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
      
        $this->add_control(
            'ectbe_additional_first_color',
            [
                'label' 		=> __( 'Label Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '                   
                    {{WRAPPER}} .ectbe-meta-additional-field dt' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this->add_control(
            'ectbe_additional_second_color',
            [
                'label' 		=> __( 'Content Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '
                    {{WRAPPER}} .ectbe-meta-additional-field dd ,
                   {{WRAPPER}} .ectbe-meta-additional-field dd a ' => 'color: {{VALUE}}',
                ],
            ]
        );

       $this->add_group_control(
			\Elementor\Group_Control_Background::get_type(),
			[
				'name' => 'ectbe_meta_background',
				'label' => __( 'Background', 'ectbe' ),
				'types' => [ 'classic', 'gradient'],
                'selector' => '{{WRAPPER}} .ectbe-meta-additional-field
               ',
			]
		);

            
      
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_additional_typography_title',
                'label' 	=> __( 'Content Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} .ectbe-meta-additional-field dt,
                    {{WRAPPER}} .ectbe-meta-additional-field dd',
            ]
        );

    

        $this->add_responsive_control(
            'ectbe_additional_padding', //param_name
            [
                'label' 		=> __( 'Padding', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} .ectbe-meta-additional-field dt,
                    {{WRAPPER}} .ectbe-meta-additional-field dd' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_additional_margin', //param_name
            [
                'label' 		=> __( 'Margin', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} .ectbe-meta-additional-field dt,
                    {{WRAPPER}} .ectbe-meta-additional-field dd' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        	$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' 		=> 'ectbe_additional_box_shadow',
				'label' 	=> __('Box Shadow', 'ectbe'),
                'selector' 	=> '{{WRAPPER}} .ectbe-meta-additional-field  ',
			]
		);

      

        $this->end_controls_section();

    }

 
    protected function render()
    {

        $id = get_the_ID();
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
            $fielchek = tribe_get_custom_fields($id);

               if(empty($fielchek)){
                
                echo'<span class="ectbe-alert-msg"><b>'. __('This widget is displayed if there are additional fields', 'ectbe').'</b></span>';


            }
        }

        $settings = $this->get_settings_for_display();
        $title=!empty($settings['ectbe_additional_custom_title'])?$settings['ectbe_additional_custom_title']:'Other';

        if (class_exists('Tribe__Events__Pro__Main')) {
            $fields = tribe_get_custom_fields($id);
   
            if(!empty($fields)){

            ?>
                    <div class="ectbe-meta-additional-field">
            	<h2 class="ectbe-events-single-section-title"> <?php esc_html_e($title, 'tribe-events-calendar-pro')?> </h2>
            	<dl>
            		<?php foreach ($fields as $name => $value): ?>
            			<dt> <?php echo esc_html($name); ?> </dt>
            			<dd class="tribe-meta-value">
            				<?php
                            // This can hold HTML. The values are cleansed upstream
                        echo $value;
                        ?>
            			</dd>
            		<?php endforeach?>
            	</dl>
            </div>
            <?php
            }
            }

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_additional_fields());
