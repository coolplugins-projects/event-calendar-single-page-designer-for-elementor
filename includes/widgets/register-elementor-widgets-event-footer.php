<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow; 

class Ectbe_widgets_event_footer extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        wp_register_style('ectbe-events-widget-style', plugin_dir_url(__FILE__) . 'css/ectbe-style.css' , '1.0.0' , 'all' );
        //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }

    public function get_style_depends(){
        return ['ectbe-events-widget-style'];
    }
/*    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }
*/
    public function get_name()
    {
        return "the-event-footer";
    }

    public function get_title()
    {
        return "Event Footer";
    }

    public function get_icon()
    {
        return 'fas fa-sticky-note';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Event Footer', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
           $this->add_control(
			'ectbe_foot_title',
			[
				'label' => __( 'Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Event Navigation', 'ectbe' ),
				
			]
		);

          $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_nav_typography',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} .ectbe-events-sub-nav a,{{WRAPPER}} .ectbe-events-sub-nav button',
            ]
        );
          	$this->add_control(
			'ectbe_foot_hiddden',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => 'traditional',
				  'selectors' => [
					'{{WRAPPER}} .ectbe-events-nav-previous button a,                    
                    {{WRAPPER}} .ectbe-events-nav-next button a' => 'color: white;',
                    '{{WRAPPER}} .ectbe-events-nav-next '=>'float:right',
                ],
			]
		);

 
        $this->add_control(
            'ectbe_nav_first_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .ectbe-events-sub-nav button,                    
                    {{WRAPPER}} .ectbe-events-sub-nav button a' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'ectbe_nav_bg_color',
            [
                'label' 		=> __( 'Background Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .ectbe-events-nav-previous button,                    
                    {{WRAPPER}} .ectbe-events-nav-next button' => 'background: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_nav_bg_hover_color',
            [
                'label' 		=> __( 'Background Hover Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .ectbe-events-nav-previous button:hover,                    
                    {{WRAPPER}} .ectbe-events-nav-next button:hover' => 'background: {{VALUE}}',
                ],
            ]
        );
    
      
        $this->add_responsive_control(
            'ectbe_nav_padding', //param_name
            [
                'label' 		=> __( 'Padding', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                       '{{WRAPPER}} .ectbe-events-sub-nav a,                    
                    {{WRAPPER}} .ectbe-events-sub-nav button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_nav_margin', //param_name
            [
                'label' 		=> __( 'Margin', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                       '{{WRAPPER}} .ectbe-events-sub-nav a,                    
                    {{WRAPPER}} .ectbe-events-sub-nav button' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();

    }

    protected function render()
    {
        $id = get_the_ID();
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
        }

        $settings = $this->get_settings_for_display();
        $title=!empty($settings['ectbe_foot_title'])?$settings['ectbe_foot_title']:'';
        $css_class = 'ectbe-evt-footer-details';
       
        $tribe_single_page_footer_op = '';
        if (function_exists('tribe_get_event_label_singular')) {
            $events_label_singular = tribe_get_event_label_singular();
        }
        if (function_exists('tribe_get_prev_event_link')) {
            $prev_event = tribe_get_prev_event_link('%title%');
        }
        if (function_exists('tribe_get_next_event_link')) {
            $next_event = tribe_get_next_event_link('%title%');
        }
        if (!empty($css_class)) {
            $tribe_single_page_footer_op .= '<div class="' . esc_attr($css_class) . '">';
        }
         $nextbtn=isset($next_event)?'<button>' . $next_event . '→</button>':'';
         $prectbebtn = isset($prev_event) ? '<button> ←' . $prev_event . ' </button>' : '';

        $tribe_single_page_footer_op .= '<div id="ectbe-events-footer">';

        $tribe_single_page_footer_op .= '<h3 class="ectbe-events-visuallyhidden">' . __($title, 'ectbe-events-calendar') . '</h3>';
        if (!empty($css_class)) {
            $tribe_single_page_footer_op .= '<span class="ectbe-events-sub-nav">';
        } else {
            $tribe_single_page_footer_op .= '<span class="ectbe-events-sub-nav">';
        }
        $tribe_single_page_footer_op .= '<span class="ectbe-events-nav-previous">'.$prectbebtn.'</span>';
        $tribe_single_page_footer_op .= '<span class="ectbe-events-nav-next" > '.$nextbtn.'</span></span></div>';

        if (!empty($css_class)) {
            $tribe_single_page_footer_op .= '</div>';
        }
        echo $tribe_single_page_footer_op;

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_event_footer());
