<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

class Ectbe_widgets_title extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

       // wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
 

    public function get_name()
    {
        return "the-event-title";
    }

    public function get_title()
    {
        return "Event Title";
    }

    public function get_icon()
    {
        return 'eicon-archive-title';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Title', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        
		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'title_typography',
				'label' 	=> __( 'Typography', 'ectbe' ),
				'selector' 	=> '{{WRAPPER}} .ectbe-event-title',
			]
		);

		$this->add_control(
			'ectbe_title',
			[
				'label' 		=> __( 'Color', 'color' ),
				'type' 			=> \Elementor\Controls_Manager::COLOR, 
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-title' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_control(
			'ectbe_title_hover',
			[
				'label' 		=> __( 'Hover Color', 'color' ),
				'type' 			=> \Elementor\Controls_Manager::COLOR, 
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-title:hover' => 'color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'ectbe_title_padding', //param_name
			[
				'label' 		=> __( 'Padding', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-title' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'ectbe_title_margin', //param_name
			[
				'label' 		=> __( 'Margin', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' 			=> 'ectbe_title_border',
                'label' 		=> __( 'Border', 'ectbe' ),
                'label_block'	=>true,
				'selector' 		=> '{{WRAPPER}} .ectbe-event-title',
			]
		);

		$this->add_control(
			'ectbe_title_shape_radius', //param_name
			[
				'label' 		=> __( 'Border Radius', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-title' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' 		=> 'ectbe_title_box_shadow',
				'label' 	=> __('Box Shadow', 'ectbe'),
				'selector' 	=> '{{WRAPPER}} .ectbe-event-title',
			]
		);	

        $this->end_controls_section();

    }

    protected function render()
    {
        GLOBAL $post;
        if ( Elementor\Plugin::$instance->editor->is_edit_mode() ) { 
            $post = get_posts( 'post_type=tribe_events&numberposts=1' );
            $post = $post[0];
        }       
        $title = '<div class="ectbe-event-title">'. $post->post_title .'</div>';
        echo $title;
    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_title());
