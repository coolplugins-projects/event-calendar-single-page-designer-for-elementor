<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;


class Ectbe_widgets_schedule_details extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

      //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }

    public function get_name()
    {
        return "the-event-schedule-details";
    }

    public function get_title()
    {
        return "Event Schedule Details";
    }

    public function get_icon()
    {
        return 'fas fa-clipboard-list';
    }

    // register controls
    protected function _register_controls()
    {
        $ectbe_admin_url = admin_url('edit.php?page=tribe-common&tab=display&post_type=tribe_events');


        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Event Schedule Details', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
			'ectbe_schedule_details_date_formats',
			[
				'label' => __( 'Date formats', 'ectbe' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
         
                            'default'=>'default',
                              'DM'=>'dM (01 Jan)',
                              'MD'=>'MD(Jan 01)',
                              'FD'=>'FD(January 01)',
                              'DF'=>'DF(01 January)',
                              'FD,Y'=>'FD,Y(January 01, 2019)',
                              'MD,Y'=>'MD,Y(Jan 01, 2019)',
                              'MD,YT'=>'MD,YT(Jan 01, 2019 8:00am-5:00pm)',
                              'full'=>'full(01 January 2019 8:00am-5:00pm)',
                              'dFY'=>'dFY(01 January 2019)',
                              'dMY'=>'dMY(01 Jan 2019)',
                    
                ],
                'description'=>__( 'Select Date Format(Please check TEC settings for Default date format)<a href = "'.$ectbe_admin_url.'">Click here </a>', 'ectbe' ),
			]
        );
        	$this->add_control(
			'ectbe_schdule_detail_color',
			[
				'label' 		=> __('Color', 'ectbe'),
				'type' 			=> \Elementor\Controls_Manager::COLOR,
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-schedule ' => 'color: {{VALUE}}',
				],
			]
		);

       	$this->add_control(
			'ectbe_schdule_detail_bg_color', //param_name
			[
				'label' 		=> __('Background Color', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-schedule' => 'background: {{VALUE}}',
				],
			]
		);
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'ectbe_schdule_detail_typo',
				'label' 	=> __('Typography', 'ectbe'),
				'selector' 	=> '{{WRAPPER}} .ectbe-events-schedule ',
			]
		);
	

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' 			=> 'ectbe_schdule_detail_border',
				'label' 		=> __('Border', 'ectbe'),
				'selector' 		=> '{{WRAPPER}} .ectbe-events-schedule',
			]
		);

		$this->add_control(
			'ectbe_schdule_detail_radius', //param_name
			[
				'label' 		=> __('Border Radius', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-schedule' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' 		=> 'ectbe_schdule_detail_shadow',
				'label' 	=> __('Box Shadow', 'ectbe'),
				'selector' 	=> '{{WRAPPER}} .ectbe-events-schedule',
			]
		);	
	
		$this->add_responsive_control(
			'ectbe_schdule_detail_padding', //param_name
			[
				'label' 		=> __('Padding', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-schedule' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
        );
        $this->add_responsive_control(
			'ectbe_schdule_detailmargin', //param_name
			[
				'label' 		=> __('Margin', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-schedule' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);	
		$this->end_controls_section();

    }

    protected function render()
    {
        $id = get_the_ID();
        if ( Elementor\Plugin::$instance->editor->is_edit_mode() ) { 
            $post = get_posts( 'post_type=tribe_events&numberposts=1' );
            $post = $post[0];
            $id = $post->ID;
        }
        
        $settings = $this->get_settings_for_display();
       
        $date_format = $settings['ectbe_schedule_details_date_formats'];
      

       $event_schedule_op = '';
       $event_id = $id;
       $ectbe_date_format = (isset($date_format) && $date_format != '' )? $date_format : 'd F Y';
       $ectbe_event_schedule = $this->ectbe_event_schedule($event_id,$ectbe_date_format);  
       $event_schedule_op .= '<div class="ectbe-events-schedule">'.$ectbe_event_schedule.'</div>';      
       echo $event_schedule_op;
    }

    private function ectbe_event_schedule($event_id,$ectbe_date_format){
        /*Date Format START*/
      $ectbe_ev_time = $this->ectbe_tribe_event_time($event_id, false);
$ectbe_event_schedule = '';

// $ectbe_ev_time=$this->ect_tribe_event_time($event_id,false);
if ($ectbe_date_format == "DM") {
    $ectbe_event_schedule = '<div class="ectbe-date"  itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'M') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "MD") {
    $ectbe_event_schedule = '<div class="ectbe-date" itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "FD") {
    $ectbe_event_schedule = '<div class="ectbe-date" itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "DF") {
    $ectbe_event_schedule = '<div class="ectbe-date" itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'F') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "FD,Y") {
    $ectbe_event_schedule = '<div class="ectbe-date"  itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_end_date($event_id, false, 'Y') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "MD,Y") {
    $ectbe_event_schedule = '<div class="ectbe-date"  itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_end_date($event_id, false, 'Y') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "MD,YT") {
    $ectbe_event_schedule = '<div class="ectbe-date" itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				<span class="ectbe-ev-time"><span class="ect-icon"><i class="ect-icon-clock" aria-hidden="true"></i></span> ' . $ectbe_ev_time . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				<span class="ectbe-ev-time">(' . tribe_get_start_date($event_id, false, 'g:i A') . ')</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . ', </span>
				<span class="ectbe-ev-yr">' . tribe_get_end_date($event_id, false, 'Y') . '</span>
				<span class="ectbe-ev-time">(' . tribe_get_end_date($event_id, false, 'g:i A') . ')</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "full") {
    $ectbe_event_schedule = '<div class="ectbe-date" itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
					<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
					<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
					<span class="ectbe-ev-time">
					<span class="ect-icon"><i class="ect-icon-clock" aria-hidden="true"></i></span> ' . $ectbe_ev_time . '</span>
					</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
					<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
					<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
					<span class="ectbe-ev-time">(' . tribe_get_start_date($event_id, false, 'g:i A') . ')</span>
					<span class="ectbe-ev-blank"> - </span>
					<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . '</span>
					<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'F') . '</span>
					<span class="ectbe-ev-yr">' . tribe_get_end_date($event_id, false, 'Y') . '</span>
					<span class="ectbe-ev-time">(' . tribe_get_end_date($event_id, false, 'g:i A') . ')</span>
					</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == "dFY") {
    $ectbe_event_schedule = '<div class="ectbe-date" itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'F') . '</span>
				<span class="ectbe-ev-yr">' . tribe_get_end_date($event_id, false, 'Y') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else if ($ectbe_date_format == 'dMY') {
    $ectbe_event_schedule = '<div class="ectbe-date" itemprop="startDate" content="' . tribe_get_start_date($event_id, false, 'Y-m-dTg:i') . '">';
    if (!tribe_event_is_multiday($event_id)) {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				</div>';
    } else {
        $ectbe_event_schedule .= '<span class="ectbe-ev-day">' . tribe_get_start_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_start_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-yr">' . tribe_get_start_date($event_id, false, 'Y') . '</span>
				<span class="ectbe-ev-blank"> - </span>
				<span class="ectbe-ev-day">' . tribe_get_end_date($event_id, false, 'd') . '</span>
				<span class="ectbe-ev-mo">' . tribe_get_end_date($event_id, false, 'M') . '</span>
				<span class="ectbe-ev-yr">' . tribe_get_end_date($event_id, false, 'Y') . '</span>
				</div>';
    }
    $ectbe_event_schedule .= '<meta itemprop="endDate" content="' . tribe_get_end_date($event_id, false, 'Y-m-dTg:i') . '">';
} else {
    $ectbe_event_schedule = '<div class="ectbe-date">' . tribe_events_event_schedule_details($event_id) . '</div>';
}
/*Date Format END*/
return $ectbe_event_schedule;

    }
        // grab events time for later use
        private function ectbe_tribe_event_time($post_id, $display = true ) {
            $event =$post_id;
            if ( tribe_event_is_all_day( $event ) ) { // all day event
                if ( $display ) {
                    _e( 'All day', 'the-events-calendar' );
                }
                else {
                    return __( 'All day', 'the-events-calendar' );
                }
            }
            elseif ( tribe_event_is_multiday( $event ) ) { // multi-date event
                $ectbe_start_date= tribe_get_start_date(  $event, false, false );
                $ectbe_end_date = tribe_get_end_date(  $event, false, false );
                if ( $display ) {
                    printf( __( '%s - %s', 'ect' ), $ectbe_start_date, $ectbe_end_date );
                }
                else {
                    return sprintf( __( '%s - %s', 'ect' ), $ectbe_start_date, $ectbe_end_date );
                }
            }
            else {
                $time_format = get_option( 'time_format' );
                $ectbe_start_date= tribe_get_start_date( $event, false, $time_format );
                $ectbe_end_date = tribe_get_end_date( $event, false, $time_format );
                if ( $ectbe_start_date!== $ectbe_end_date ) {
                    if ( $display ) {
                        printf( __( '%s - %s', 'ect' ), $ectbe_start_date, $ectbe_end_date );
                    }
                    else {
                        return sprintf( __( '%s - %s', 'ect' ), $ectbe_start_date, $ectbe_end_date );
                    }
                }
                else {
                    if ( $display ) {
                        printf( '%s', $ectbe_start_date);
                    }
                    else {
                        return sprintf( '%s', $ectbe_start_date);
                    }
                }
            }
        }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_schedule_details());
