<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow; 

class Ectbe_widgets_rsvp_form extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);
       // wp_dequeue_script( 'tribe-tickets-rsvp-block','tribe-tickets-rsvp-ari');
       /* wp_dequeue_script('tribe-tickets-rsvp-block');
wp_deregister_script('tribe-tickets-rsvp-block');
      wp_dequeue_script('tribe-tickets-rsvp-ari');
wp_deregister_script('tribe-tickets-rsvp-ari'); */


        
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
   /*  public function get_script_depends()
    {       
            $style=[];
        if (\Elementor\Plugin::$instance->editor->is_edit_mode() || \Elementor\Plugin::$instance->preview->is_preview_mode()) {
            return [];
        }
        return $style;
    
    }   */
    public function get_style_depends() {
        $style=[];
        if (\Elementor\Plugin::$instance->editor->is_edit_mode() || \Elementor\Plugin::$instance->preview->is_preview_mode()) {
            return [ tribe_asset_enqueue('tribe-tickets-rsvp-style','tribe-tickets-form-style','tribe-tickets-rsvp','tribe-common-responsive')];
        }
        return $style;
    }
 
    public function get_name()
    {
        return "the-event-rsvp-form";
    }

    public function get_title()
    {
        return "Event RSVP Form";
    }

    public function get_icon()
    {
        return 'fas fa-envelope-open-text';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_rsvp_section',
            [
                'label' => __('Event RSVP Form', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
            $this->add_control(
			'ectbe_rsvp_title_heading',
			[
				'label' => __( 'Title ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
           $this->add_control(
            'ectbe_rsvp__title_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-tickets__rsvp-wrapper h3   ' => 'color: {{VALUE}}',
                ],
            ]
        );
                $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_rsvp_typography',
                'label' 	=> __( 'Title Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} .tribe-tickets__rsvp-wrapper h3',
            ]
        );
               $this->add_control(
			'ectbe_rsvp_content_heading',
			[
				'label' => __( 'Content ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
          $this->add_control(
            'ectbe_rsvp_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-tickets__rsvp-wrapper p,
                  
                    {{WRAPPER}} .tribe-tickets__rsvp-wrapper span,
                    {{WRAPPER}} .tribe-tickets__rsvp-wrapper div' => 'color: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_rsvp_bg_color',
            [
                'label' 		=> __( 'Background Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-tickets__rsvp-wrapper' => 'background: {{VALUE}}',
                ],
            ]
        );
        
      
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_rsvp_text_typography',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>  '{{WRAPPER}} .tribe-tickets__rsvp-wrapper p,
                    
                    {{WRAPPER}} .tribe-tickets__rsvp-wrapper span,
                    {{WRAPPER}} .tribe-tickets__rsvp-wrapper div',
            ]
        );
    

               $this->add_control(
			'ectbe_content_button_heading',
			[
				'label' => __( 'Button ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
               $this->add_control(
            'ectbe_rsvp_btn_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-tickets__rsvp-actions-rsvp-going button ' => 'color: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_rsvp_btn_bg_color',
            [
                'label' 		=> __( 'Background Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-tickets__rsvp-actions-rsvp-going button ' => 'background: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_rsvp_btn_bg_hover_color',
            [
                'label' 		=> __( 'Background Hover Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .tribe-tickets__rsvp-actions-rsvp-going button:hover ' => 'background: {{VALUE}}',
                ],
            ]
        );

       
      
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_rsvp_button_typography',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>  '{{WRAPPER}} .tribe-tickets__rsvp-actions-rsvp-going button  ',
            ]
        );
       
    

        $this->end_controls_section();

    }


    protected function render()
    {       
        $id = get_the_ID();
        $post = '';

        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
             if(empty(Tribe__Tickets__Tickets_View::instance()->get_rsvp_block($id,false))){                
                echo'<span class="ectbe-alert-msg"><b>'. __('This widget is displayed if RSVP is set in this page.', 'ectbe').'</b></span>';
            }  
            
        }                   
        
        if (class_exists('Tribe__Tickets__Main')) {
        Tribe__Tickets__Tickets_View::instance()->get_rsvp_block($id);
        }   
        }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_rsvp_form());
