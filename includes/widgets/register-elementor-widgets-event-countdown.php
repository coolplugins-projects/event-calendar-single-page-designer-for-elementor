<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow; 

class Ectbe_widgets_event_countdown extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        wp_register_script( 'ectbe-events-countdown-widget', plugin_dir_url(__FILE__) .'js/ectbe-countdown-widget.js' , '1.0.0', true );
        wp_register_style('ectbe-count-style', plugin_dir_url(__FILE__) . 'css/ectbe-style.css', '1.0.0', 'all');

        wp_register_script('ectbe-custom-counter',  plugin_dir_url(__FILE__) .'js/ectbe-custom.js', ['elementor-frontend'], null, true);

    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-countdown-widget','ectbe-custom-counter'];
    }
    public function get_style_depends(){
        return ['ectbe-count-style'];
    }

    public function get_name()
    {
        return "the-event-countdown";
    }

    public function get_title()
    {
        return "Event Countdown";
    }

    public function get_icon()
    {
        return 'fas fa-stopwatch';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Event Countdown', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
   
        $this->add_control(
            'ectbe_counter_color',
            [
                'label' 		=> __( 'Date Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                   '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number ' => 'color: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_date_label_color',
            [
                'label' 		=> __( 'Date Label Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                   '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number span.ectbe-countdown-under ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number span.ectbe-countdown-under ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number span.ectbe-countdown-under ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number span.ectbe-countdown-under ' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'ectbe_counter_bg_color',
            [
                'label' 		=> __( 'Background Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number ' => 'background: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_counter_bg_hover_color',
            [
                'label' 		=> __( 'Background Hover Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                     '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number:hover ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number:hover ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number:hover ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number:hover ' => 'background: {{VALUE}}',
                ],
            ]
        );
    
   
            
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_count_typography',
                'label' 	=> __( 'Time Typography', 'ectbe' ),
                'selector' 	=> '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number ',
            ]
        );
           $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_count_label_typography',
                'label' 	=> __( 'Time Label Typography', 'ectbe' ),
                'selector' 	=> '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number span.ectbe-countdown-under ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number span.ectbe-countdown-under ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number span.ectbe-countdown-under ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number span.ectbe-countdown-under ',
            ]
        );
 
        $this->add_responsive_control(
            'ectbe_counter_padding', //param_name
            [
                'label' 		=> __( 'Padding', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                       '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number '=> 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_counter_margin', //param_name
            [
                'label' 		=> __( 'Margin', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} .ectbe-countdown-days.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-hours.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-minutes.ectbe-countdown-number ,
                    {{WRAPPER}} .ectbe-countdown-seconds.ectbe-countdown-number ' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

      
      	$this->add_control(
			'content__passed_section',
			[
				'label' => __( 'Passed & Ongoing Event Message Settings', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        $this->add_control(
			'ectbe_ongoing_event_title',
			[
				'label' => __( 'Ongoing Event Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'The event is ongoing.', 'ectbe' ),
				
			]
        );
        	$this->add_control(
			'ectbe_passed_event_title',
			[
				'label' => __( 'Passed Event Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'This event has been passed', 'ectbe' ),
				
			]
        );

               $this->add_control(
            'ectbe_date_label_passed_color',
            [
                'label' 		=> __( 'Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                   '{{WRAPPER}} #ectbe_passed_event' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'ectbe_counter_passed_bg_color',
            [
                'label' 		=> __( 'Background Color', 'color' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} #ectbe_passed_event'  => 'background: {{VALUE}}',
                ],
            ]
        );
          $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_count_passed_typography',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=> '{{WRAPPER}} #ectbe_passed_event' ,
            ]
        );
             $this->add_responsive_control(
            'ectbe_counter_passed_padding', //param_name
            [
                'label' 		=> __( 'Padding', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                       '{{WRAPPER}} #ectbe_passed_event'=> 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_counter_passed_margin', //param_name
            [
                'label' 		=> __( 'Margin', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} #ectbe_passed_event' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->end_controls_section();


    }

    protected function render()
    {

       

        $single_page_meta_event_map_op = '';
        $id = '';
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
           
         //   wp_enqueue_script('ectbe-events-countdown-widget');

        }else{
            $id = get_the_ID();

        }

        $settings = $this->get_settings_for_display();
        $css_class = 'ectbe-events-countdown';
        
        $passed_title =!empty($settings['ectbe_passed_event_title'])?$settings['ectbe_passed_event_title']:'';
        $ongoing_title = !empty($settings['ectbe_ongoing_event_title']) ? $settings['ectbe_ongoing_event_title'] : '';

        $tribe_single_page_countdown_op = '';
        $seconds = '';

        ob_start();
        
        if( !empty($css_class)){
        echo '<div class="'.esc_attr($css_class).'">';
        }
        
        // Get the event start date.
        if(function_exists('tribe_get_start_date')){
        $ectbe_start_date= tribe_get_start_date( $id, false, 'Y/m/d H:i:s' );
        // Get the number of seconds remaining until the date in question.
        $seconds = strtotime( $ectbe_start_date) - current_time( 'timestamp' );
      
        

        }
        
        if( $seconds > 0 ){
           
        ?>
            <div class="tec-tribe-single-event-countdown">
                <?php ob_start(); 
            
                if(!empty($css_class) ){
                    echo '<div class="ectbe-countdown-timer ">';
                }
                else{
                    echo '<div class="ectbe-countdown-timer ">';
                }
                ?>
                    <div class="ectbe-countdown-days ectbe-countdown-number">DD<br />
                        <span class="ectbe-countdown-under"><?php esc_html_e( 'days', 'ticketbox' ); ?></span>
                    </div>
                   
                    <div class="ectbe-countdown-hours ectbe-countdown-number">HH<br />
                        <span class="ectbe-countdown-under"><?php esc_html_e( 'hours', 'ticketbox' ); ?></span>
                    </div>
                   
                    <div class="ectbe-countdown-minutes ectbe-countdown-number">MM<br />
                        <span class="ectbe-countdown-under"><?php esc_html_e( 'min', 'ticketbox' ); ?></span>
                    </div>
                    
                    <div class="ectbe-countdown-seconds ectbe-countdown-number ectbe-countdown-right">SS<br />
                        <span class="ectbe-countdown-under"><?php esc_html_e( 'sec', 'ticketbox' ); ?></span>
                    </div>
                </div>
                <?php
                $hourformat = ob_get_clean();
            
                ob_start();
                ?>
                <div class="countdown-wrap">
                    <div class="ectbe-countdown-timer">
                        <span class="ectbe-countdown-seconds"><?php echo $seconds; ?></span>
                        <span class="ectbe-countdown-format"><?php echo $hourformat ?></span>
                    </div>
                </div>
                <?php
                echo ob_get_clean();
                ?>
            </div>
        <?php
        }
        else{    
        $curentdate=date('Y/m/d H:i:s');
        $end_date=tribe_get_end_date($id, false, 'Y/m/d H:i:s' );
        $dateTimestamp1 = strtotime($curentdate);
        $dateTimestamp2 = strtotime($end_date);



     if($dateTimestamp1 <=$dateTimestamp2){
         echo '<h4 id="ectbe_passed_event">'.$ongoing_title.'</h4>';
     }
      else{
        echo'<h4 id="ectbe_passed_event">'.$passed_title .'</h4>';
        }
      
    }        
        
    if( !empty($css_class) ){
    echo '</div>';
    }
    
    echo ob_get_clean();

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_event_countdown());
