<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;

class Ectbe_widgets_schedule_ticket_cost extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }

    public function get_name()
    {
        return "the-event-ticket-cost";
    }

    public function get_title()
    {
        return "Event Cost";
    }

    public function get_icon()
    {
        return 'fas fa-file-invoice-dollar';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Event Cost', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        	$this->add_control(
			'ectbe_cost_color',
			[
				'label' 		=> __('Color', 'ectbe'),
				'type' 			=> \Elementor\Controls_Manager::COLOR,
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-cost span ' => 'color: {{VALUE}}',
				],
			]
		);

       	$this->add_control(
			'ectbe_cost_bg_color', //param_name
			[
				'label' 		=> __('Background Color', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-cost span' => 'background: {{VALUE}}',
				],
			]
		);
        $this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' 		=> 'ectbe_cost_typo',
				'label' 	=> __('Schdule Detail Typography', 'ectbe'),
				'selector' 	=> '{{WRAPPER}} .ectbe-events-cost span ',
			]
        );
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' 			=> 'ectbe_cost_border',
				'label' 		=> __('Border', 'ectbe'),
				'selector' 		=> '{{WRAPPER}} .ectbe-events-cost span',
			]
		);
		$this->add_control(
			'ectbe_cost_radius', //param_name
			[
				'label' 		=> __('Border Radius', 'ectbe'), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> ['px', 'em', '%'],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-events-cost span' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
        $this->end_controls_section();

    }

    protected function render()
    {
        $id = get_the_ID();
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
        }
        $settings = $this->get_settings_for_display();
        $single_page_cost_op = "";       
        $single_page_cost_op .= '<div class="ectbe-events-cost">';
        if (function_exists('tribe_get_cost')) {
            if (tribe_get_cost($id,true)) {
                $single_page_cost_op .= '<span class="ectbe-events-cost">' . tribe_get_cost($id, true) . '</span>';
            }else{
                $single_page_cost_op .= '<span class="ectbe-events-cost">' . __('--','ectbe') . '</span>';
            }
        }
        $single_page_cost_op .= '</div>';
        echo $single_page_cost_op;
    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_schedule_ticket_cost());
