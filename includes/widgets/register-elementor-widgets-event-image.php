<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Css_Filter;


class Ectbe_widgets_schedule_event_image extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }

    public function get_name()
    {
        return "the-event-event-image";
    }

    public function get_title()
    {
        return "Event Image";
    }

    public function get_icon()
    {
        return 'fas fa-file-image';
    }

    // register controls
    protected function _register_controls()
    {

        

        $this->start_controls_section(
            'content_section',
            [
                'label' => __('Event Feature image', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
		);
		$this->add_control(
			'view_image_hover',
			[
				'label' => __( 'View', 'plugin-domain' ),
				'type' => \Elementor\Controls_Manager::HIDDEN,
				'default' => 'traditional',
				  'selectors' => [
					'{{WRAPPER}} .ectbe-featured-img img' => 'transition: 0.5s;',
					 '{{WRAPPER}} .ectbe-featured-img ' => 'overflow: hidden;',
					'{{WRAPPER}} .ectbe-featured-img img:hover' => 'transform: scale(1.1);',
                ],
			]
		);
  
            
        $this->add_responsive_control(
            'ectbe_featured_image_width',
            [
                'label' => __('Image Width', 'ectbe'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 200,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 0,
                        'max' => 100,
                    ],
                ],
                'default' => [
                    'unit' => '%',
                    'size' => 100,
                ],
                'selectors' => [
                    '{{WRAPPER}} .ectbe-featured-img img' => 'width: {{SIZE}}{{UNIT}}; height: auto;',
                ],
            ]
        );
        $this->add_responsive_control(
        			'ectbe_featured_image_height',
        			[
				'label' 		=> __( 'Image Height', 'ectbe' ),
				'type' 			=> Controls_Manager::SLIDER,
				'size_units' 	=> [ 'px', '%' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
						'step' => 1,
					],
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-featured-img img' => 'height: {{SIZE}}{{UNIT}};  width: auto;',
				],
			]
		);


			$this->add_control(
			'ectbe_featured_image_box_border_radius',
			[
				'label' 		=> __( 'Border Radius', 'ectbe' ),
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS,
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-featured-img img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);
		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' 			=> 'ectbe_featured_image_box_border',
				'label' 		=> __( 'Border', 'ectbe' ),
				'selector' 		=> '{{WRAPPER}} .ectbe-featured-img img',
			]
		);

	



		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name'     => 'css_filters',
				'label' 	=> __('Overlay Style', 'ectbe'),
				'selector' => '{{WRAPPER}} .ectbe-featured-img img',
			]
		);
	

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name'     => 'css_filters_hover',
				'label' 	=> __('Hover Overlay Style', 'ectbe'),
				'selector' => '{{WRAPPER}} .ectbe-featured-img img:hover',
			]
		);	
		  	$this->add_control(
			'ectbe_img_btn_heading',
			[
				'label' => __( 'Container ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
				$this->add_control(
			'ectbe_featured_image_box_bg_color', //param_name
			[
				'label' 		=> __( 'Background Color', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-featured-img' => 'background: {{VALUE}}',
				],
			]
		);
        		$this->add_responsive_control(
			'ectbe_featured_image_box_padding', //param_name
			[
				'label' 		=> __( 'Padding', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-featured-img' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'ectbe_featured_image_box_margin', //param_name
			[
				'label' 		=> __( 'Margin', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
				'size_units' 	=> [ 'px', 'em', '%' ],
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-featured-img' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

        $this->end_controls_section();

    }

    protected function render()
    {
        $id = get_the_ID();
        if(Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
        }
		$settings = $this->get_settings_for_display();

        $css_class = 'ectbe-event-image';
        $image_size = 'full';

        $ectbe_pageid    = get_post_meta( $id , 'tec_tribe_single_event_page', true);
        $ectbe_pagetitle = str_replace(' ','-', get_the_title($ectbe_pageid) );
        if(function_exists('tribe_event_featured_image')){
            $tribe_featured_img = !empty(tribe_event_featured_image( $id, $image_size, false ))?tribe_event_featured_image( $id, $image_size, false ): ' ';
        }
    
        $single_page_img_op = '';
        if( !empty($el_class) ){
            $single_page_img_op .= '<div class="'.esc_attr($css_class).'">';
        }
        $single_page_img_op .= '<div class="ectbe-featured-img '.$ectbe_pagetitle.'">'.$tribe_featured_img.'</div>';
          if( !empty($el_class) ) {
            $single_page_img_op .= '</div>';
        }
        
        echo $single_page_img_op;
	


        
    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_schedule_event_image());
