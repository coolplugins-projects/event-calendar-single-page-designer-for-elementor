<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
use Elementor\Group_Control_Typography;


class Ectbe_widgets_event_sharebutton extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

       
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
  

    public function get_name()
    {
        return "the-event-sharebtn";
    }

    public function get_title()
    {
        return "Share Event";
    }

    public function get_icon()
    {
        return 'fa fa-share-alt';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'content_share_section',
            [
                'label' => __('Share Event', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
           $this->add_control(
			'ectbe_share_custom_title',
			[
				'label' => __( 'Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Share This Event', 'ectbe' ),
                'placeholder' => __( 'Type your title here', 'ectbe' ),
                
			]
        );
        	$this->add_control(
			'ectbe_title_share_color', //param_name
			[
				'label' 		=> __( 'Title Color', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-share_event h3' => 'color: {{VALUE}}',
				],
			]
        );
        	$this->add_control(
			'ectbe_featured_share_color', //param_name
			[
				'label' 		=> __( 'Icon Color', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-share-link' => 'color: {{VALUE}}',
				],
			]
        );
        	$this->add_control(
			'ectbe_featured_share_hover_color', //param_name
			[
				'label' 		=> __( 'Icon Hover Color', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-share-link:hover' => 'color: {{VALUE}}',
				],
			]
        );
        
        	$this->add_control(
			'ectbe_featured_bg_share_color', //param_name
			[
				'label' 		=> __( 'Background Color', 'ectbe' ), //heading
				'type' 			=> \Elementor\Controls_Manager::COLOR, //type
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-share_event' => 'background: {{VALUE}}',
				],
			]
		);
         $this->add_responsive_control(
            'ectbe_featured_share_size',
            [
                'label' => __('Icon Size', 'ectbe'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px'],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 100,
                        'step' => 1,
                    ]                   
                ],
                'default' => [
                    'unit' => 'px',
                    'size' => 50,
                ],
                'selectors' => [
                    '{{WRAPPER}} .ectbe-share-link' => 'font-size: {{SIZE}}{{UNIT}};',
                ],
            ]
        );
          $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ectbe_typography_share_title',
                'label' => __('Typography', 'ectbe'),
                'selector' => '{{WRAPPER}} .ectbe-event-share_event h3',
            ]
        );
        $this->add_responsive_control(
            'ectbe_share_box_padding', //param_name
            [
                'label' => __('Padding', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ectbe-share-link' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_share_box_margin', //param_name
            [
                'label' => __('Margin', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '{{WRAPPER}} .ectbe-share-link' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        	$this->add_control(
			'ectbe_share_align',
			[
				'label' => __( 'Alignment', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'ectbe' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'ectbe' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'ectbe' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'default' => 'center',
				'toggle' => true,
				'selectors' 	=> [
					'{{WRAPPER}} .ectbe-event-share_event' => 'text-align: {{VALUE}}',
				],
			]
		);

        $this->end_controls_section();

    }

    /**
     * Get Meta Map template
     */
  function ectbe_share_button($event_id,$title){
	
	wp_enqueue_script('ectbe-sharebutton', ECTBE_PLUGIN_URL .'/includes/widgets/js/ectbe-sharebutton.js',array('jquery'),null,true);
	
	$ectbe_sharecontent = '';
	$ectbe_get_url = urlencode(get_permalink($event_id));
	
	$ectbe_gettitle = htmlspecialchars(urlencode(html_entity_decode(get_the_title($event_id), ENT_COMPAT, 'UTF-8')), ENT_COMPAT, 'UTF-8');
	$ectbe_getthumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $event_id ), 'full' );
	$subject= str_replace("+"," ",$ectbe_gettitle);
	// Construct sharing URL
		$ectbe_twitterURL = 'https://twitter.com/intent/tweet?text='.$ectbe_gettitle.'&amp;url='.$ectbe_get_url.'';
		$ectbe_whatsappURL = 'https://wa.me/?text='.$ectbe_gettitle . ' ' . $ectbe_get_url;
		$ectbe_facebookurl = 'https://www.facebook.com/sharer/sharer.php?u='.$ectbe_get_url.'';
		$ectbe_emailUrl = 'mailto:?Subject='.$subject.'&Body='.$ectbe_get_url.'';
        $ectbe_sharecontent .= '<h3 class="ectbe-share-title">'. __($title,'ectbe').'</h3>';
		$ectbe_sharecontent .= '<a class="ectbe-share-link" href="'.$ectbe_facebookurl.'" target="_blank" title="Facebook" aria-haspopup="true"><i class="fab fa-facebook-f"></i></a>';
		$ectbe_sharecontent .= '<a class="ectbe-share-link" href="'.$ectbe_twitterURL.'" target="_blank" title="Twitter" aria-haspopup="true"><i class="fab fa-twitter"></i></a>';
		// $ectbe_sharecontent .= '<a class="ect-share-link" href="'.$ect_linkedinUrl.'" target="_blank" title="Linkedin" aria-haspopup="true"><i class="ect-icon-linkedin"></i></a>';
		$ectbe_sharecontent .= '<a class="ectbe-share-link" href="'.$ectbe_emailUrl.' "title="Email" aria-haspopup="true"><i class="fas fa-envelope-square"></i></a>';
		$ectbe_sharecontent .= '<a class="ectbe-share-link" href="'.$ectbe_whatsappURL.'" target="_blank" title="WhatsApp" aria-haspopup="true"><i class="fa fa-whatsapp"></i></a>';
		return $ectbe_sharecontent;
}
    protected function render()
    {

        $single_page_meta_event_share = '';
        $id = get_the_ID();
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
        }

        $settings = $this->get_settings_for_display();
        $title=!empty($settings['ectbe_share_custom_title'])?$settings['ectbe_share_custom_title']:'';
        $css_class = 'ectbe-event-share_event';
        echo '<style>
        .ectbe-share-link{
                 padding: 15px;
                  
                    }
        </style>';

        if (!empty($css_class)) {
            $single_page_meta_event_share .= '<div class="' . esc_attr($css_class) . '">';
        }
        $single_page_meta_event_share .= $this->ectbe_share_button($id,$title);
        if (!empty($css_class)) {
            $single_page_meta_event_share .= '</div>';
        }

        echo $single_page_meta_event_share;

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_event_sharebutton());
