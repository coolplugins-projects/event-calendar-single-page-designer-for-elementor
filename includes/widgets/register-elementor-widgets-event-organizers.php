<?php
use Elementor\Controls_Manager;
use Elementor\Group_Control_Box_Shadow;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

class Ectbe_widgets_event_organizer extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);

        //  wp_register_script( 'ectbe-events-widget', plugin_dir_url(__FILE__) .'js/ectbe-title-widget.js' , [ 'elementor-frontend' ], '1.0.0', true );
    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
    public function get_script_depends()
    {
        return ['ectbe-events-widget'];
    }

    public function get_name()
    {
        return "the-event-event-organizer";
    }

    public function get_title()
    {
        return "Event Organizer";
    }

    public function get_icon()
    {
        return 'fas fa-user-tie';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'org_content_section',
            [
                'label' => __('Event Organizer', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
        $this->add_control(
			'ectbe_enable_custom_org_txt',
			[
				'label' => __( 'Enable Custom Labels', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::SWITCHER,
				'label_on' => __( 'Show', 'your-plugin' ),
				'label_off' => __( 'Hide', 'your-plugin' ),
				'return_value' => 'yes',
				'default' => 'no',
			]
		);
        $this->add_control(
			'ectbe_org_custom_title',
			[
				'label' => __( 'Title', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Organizer', 'ectbe' ),
                'placeholder' => __( 'Type your title here', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_org_txt' => 'yes'],
			]
        );
          $this->add_control(
			'ectbe_org_custom_name',
			[
					'label' => __( 'Organizer name', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Organizer', 'ectbe' ),
                'placeholder' => __( 'Type your email label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_org_txt' => 'yes'],
			]
        );
          
		
        $this->add_control(
			'ectbe_org_custom_email',
			[
					'label' => __( 'Email label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Email:', 'ectbe' ),
                'placeholder' => __( 'Type your email label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_org_txt' => 'yes'],
			]
        );
          $this->add_control(
			'ectbe_org_custom_phone',
			[
				'label' => __( 'Phone Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Phone:', 'ectbe' ),
                'placeholder' => __( 'Type your Phone Label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_org_txt' => 'yes'],
			]
        );
           $this->add_control(
			'ectbe_org_custom_label',
			[
				'label' => __( 'Website Label', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::TEXT,
				'default' => __( 'Website:', 'ectbe' ),
                'placeholder' => __( 'Type your website label', 'ectbe' ),
                'condition' => ['ectbe_enable_custom_org_txt' => 'yes'],
			]
        );
        	$this->add_control(
			'ectbe_org_btn_heading',
			[
				'label' => __( 'Title ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
              $this->add_control(
            'ectbe_title_org_color',
            [
                'label' => __('Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '
                    {{WRAPPER}} .tect-organizer h2,
                    {{WRAPPER}} .ectbe-wrp-organizer h2' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this->add_control(
            'ectbe_title_org_bg_color',
            [
                'label' => __('Background Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '
                    {{WRAPPER}} .tect-organizer h2,
                    {{WRAPPER}} .ectbe-wrp-organizer h2' => 'background: {{VALUE}}',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ectbe_org_typography',
                'label' => __('Typography', 'ectbe'),
                'selector' => '{{WRAPPER}} .tect-organizer h2,{{WRAPPER}} .ectbe-wrp-organizer h2',
            ]
        );
         	$this->add_control(
			'ectbe_org_conten_heading',
			[
				'label' => __( 'Content ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'ectbe_typography_title',
                'label' => __('Typography', 'ectbe'),
                'selector' => '{{WRAPPER}} .tect-organizer dt,
                    {{WRAPPER}} .tect-organizer dd,
                    {{WRAPPER}} .tect-organizer dd a,
                    {{WRAPPER}} .ectbe-wrp-organizer dt,
                    {{WRAPPER}} .ectbe-wrp-organizer dd,
                    {{WRAPPER}} .ectbe-wrp-organizer dd a
                    ',
            ]
        );
        $this->add_control(
            'ectbe_titl_hiddn_width',
            [
                'label' => __('View', 'ectbe'),
                'type' => \Elementor\Controls_Manager::HIDDEN,
                'default' => 'traditional',
                'selectors' => [
                    '{{WRAPPER}} .ectbe-wrp-organizer h2,
                    {{WRAPPER}} .tect-organizer h2' => 'width: 100%',
                ],

            ]
        );

        $this->add_control(
            'ectbe_title_first_color',
            [
                'label' => __('Label Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .tect-organizer dt ,
                    {{WRAPPER}} .ectbe-wrp-organizer dt ' => 'color: {{VALUE}}',
                ],
            ]
        );

        $this->add_control(
            'ectbe_title_color',
            [
                'label' => __('Content Color', 'ectbe'),
                'type' => \Elementor\Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .tect-organizer dd,
                    {{WRAPPER}} .tect-organizer dd a,

                    {{WRAPPER}} .ectbe-wrp-organizer dd,
                    {{WRAPPER}} .ectbe-wrp-organizer dd a' => 'color: {{VALUE}}',
                ],
            ]
        );
        $this->add_group_control(
            \Elementor\Group_Control_Background::get_type(),
            [
                'name' => 'ectbe_org_background',
                'label' => __('Background', 'ectbe'),
                'types' => ['classic', 'gradient'],
                'selector' => '{{WRAPPER}} .tect-organizer,{{WRAPPER}} .ectbe-wrp-organizer',
            ]
        );

        $this->add_responsive_control(
            'ectbe_org_padding', //param_name
            [
                'label' => __('Padding', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '{{WRAPPER}} .tect-organizer dt,
                    {{WRAPPER}} .tect-organizer dd,
                    {{WRAPPER}} .tect-organizer dd a,

                     {{WRAPPER}} .ectbe-wrp-organizer dt,
                    {{WRAPPER}} .ectbe-wrp-organizer dd,
                    {{WRAPPER}} .ectbe-wrp-organizer dd a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_org_margin', //param_name
            [
                'label' => __('Margin', 'ectbe'), //heading
                'type' => \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' => ['px', 'em', '%'],
                'selectors' => [
                    '{{WRAPPER}} .tect-organizer dt,
                    {{WRAPPER}} .tect-organizer dd,
                    {{WRAPPER}} .tect-organizer dd a,

                     {{WRAPPER}} .ectbe-wrp-organizer dt,
                    {{WRAPPER}} .ectbe-wrp-organizer dd,
                    {{WRAPPER}} .ectbe-wrp-organizer dd a' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        $this->add_group_control(
            Group_Control_Box_Shadow::get_type(),
            [
                'name' => 'ectbe_org_box_box_shadow',
                'label' => __('Box Shadow', 'ectbe'),
                'selector' => '{{WRAPPER}} .tect-organizer, {{WRAPPER}} .ectbe-wrp-organizer',
            ]
        );

        $this->end_controls_section();

    }

 
    protected function render()
    {
        $id = get_the_ID();
            $settings = $this->get_settings_for_display();
        $titlelbl = !empty($settings['ectbe_org_custom_title']) ? $settings['ectbe_org_custom_title'] : '';
   
     

        $org_nm_lb=!empty($settings['ectbe_org_custom_name']) ? $settings['ectbe_org_custom_name'] : '';

        $email_lbl=!empty($settings['ectbe_org_custom_email'])?$settings['ectbe_org_custom_email']:'Email:';
        $phone_lbl=!empty($settings['ectbe_org_custom_phone'])?$settings['ectbe_org_custom_phone']:'Phone:';
      
        $wesite_label = !empty($settings['ectbe_org_custom_label']) ? $settings['ectbe_org_custom_label'] : 'Website:';
       
       
      
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
               if(empty(tribe_get_organizer_phone($id)||tribe_get_organizer_email($id)||tribe_get_organizer_website_link($id))){
                
                echo'<span class="ectbe-alert-msg"><b>'. __('This widget is displayed if organizer is set in this page.', 'ectbe').'</b></span>';


            }

        }

            $organizer_ids = tribe_get_organizer_ids($id);
            $multiple = count($organizer_ids) > 1;
            $title=($settings['ectbe_enable_custom_org_txt'] == "yes") ?$titlelbl : tribe_get_organizer_label(!$multiple);

        //    var_dump(tribe_get_organizer_label(!$multiple));

            $phone =tribe_get_organizer_phone($id);
            $email = tribe_get_organizer_email($id);

            $website =tribe_get_organizer_website_link($id);
            if(!empty($phone||$email||$website)){
            ?>
<div class="ectbe-wrp-organizer">
	<h2 class="ectbe-evt-single-title"><?php echo !empty($phone||$email||$website)?$title:''; ?></h2>
	<dl>
		<?php
          
            foreach ($organizer_ids as $organizer) {
                if (!$organizer) {
                    continue;
                }

                ?>
			<dt><?php esc_html_e($org_nm_lb, 'the-events-calendar')?></dt>
			<dd class="ectbe-organizer">
				<?php echo tribe_get_organizer_link($organizer) ?>
			</dd>
			<?php
            }
        

            if (!$multiple) { // only show organizer details if there is one
                if (!empty($phone)) {
                    ?>
				<dt class="ectbe-org-tel-label">
					<?php esc_html_e($phone_lbl, 'the-events-calendar')?>
				</dt>
				<dd class="ectbe-organizer-tel">
					<?php echo esc_html($phone); ?>
				</dd>
				<?php
            } //end if

                if (!empty($email)) {
                    ?>
				<dt class="ectbe-org-email-label">
					<?php esc_html_e($email_lbl, 'the-events-calendar')?>
				</dt>
				<dd class="ectbe-organizer-email">
					<?php echo esc_html($email); ?>
				</dd>
				<?php
            } //end if

                if (!empty($website)) {
                    ?>
				<dt class="ectbe-organizer-url-label">
					<?php esc_html_e($wesite_label, 'the-events-calendar')?>
				</dt>
				<dd class="ectbe-organizer-url">
					<?php echo $website; ?>
				</dd>
				<?php
            } //end if
            } //end if

            ?>
	</dl>
</div>


<?php
            }

    }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_event_organizer());
