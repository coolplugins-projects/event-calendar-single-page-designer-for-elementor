<?php
use Elementor\Controls_Manager;
use Elementor\Widget_Base;
 use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow; 
use Elementor\Group_Control_Background ;

class Ectbe_widgets_registration_form extends \Elementor\Widget_Base
{

    public function __construct($data = [], $args = null)
    {
        // must call the parent class constructor
        parent::__construct($data, $args);
        wp_register_script('ectbe_reg_form', plugin_dir_url(__FILE__) . 'js/ectbe-reg-form.js', ['elementor-frontend'], null, true);

    }
      public function get_script_depends()
    {
       
         return ['ectbe_reg_form'];

    }

    public function get_categories()
    {
        return ['ectbe_ect_single_addons'];
    }
/*    public function get_script_depends()
{
return ['ectbe-events-countdown-widget'];
}
 */
    public function get_name()
    {
        return "the-event-registration-form";
    }

    public function get_title()
    {
        return "Event Registration Form";
    }

    public function get_icon()
    {
        return 'fab fa-wpforms';
    }

    // register controls
    protected function _register_controls()
    {

        $this->start_controls_section(
            'ectbe_form_content_section',
            [
                'label' => __('Event Registration Form', 'ectbe'),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );
     
        $this->add_control(
			'ectbe_content_heading',
			[
				'label' => __( 'Content ', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
       
         $this->add_control(
            'ectbe_form_first_color',
            [
                'label' 		=> __( 'Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                
                'selectors' 	=> [
                    '{{WRAPPER}} #rtec label,
                     {{WRAPPER}} #rtec h3,
                    {{WRAPPER}} ' => 'color: {{VALUE}}',
                ],
            ]
        );
             $this->add_control(
            'ectbe_form__bg_color',
            [
                'label' 		=> __( 'Background Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    '{{WRAPPER}} #rtec h3,
                    {{WRAPPER}} #rtec ,
                   {{WRAPPER}} #rtec .rtec-form-wrapper' => 'background: {{VALUE}}',
                ],
            ]
        );
              $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_form_typography_content',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>   '{{WRAPPER}} #rtec label',
            ]
        );
  
           $this->add_control(
			'ectbe_content_burron_heading',
			[
				'label' => __( 'Button', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
 
     
            $this->add_control(
            'ectbe_form_btn__color',
            [
                'label' 		=> __( 'Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    ' {{WRAPPER}} #rtec [name="rtec_submit"],
                   {{WRAPPER}} #rtec-form-toggle-button,
        
                 {{WRAPPER}} #ectbe_reg_form,
                   {{WRAPPER}} #rtec [name="rtec_visitor_submit"]' => 'color: {{VALUE}}',
                ],
            ]
        );
         $this->add_control(
            'ectbe_form_btn_bg_color',
            [
                'label' 		=> __( 'Background Color', 'ectbe' ),
                'type' 			=> \Elementor\Controls_Manager::COLOR, 
                'selectors' 	=> [
                    ' {{WRAPPER}} #rtec [name="rtec_submit"],
                   {{WRAPPER}} #rtec-form-toggle-button,
                   {{WRAPPER}} #ectbe_reg_form,
                   {{WRAPPER}} #rtec [name="rtec_visitor_submit"]' => 'background: {{VALUE}}',
                ],
            ]
        );
      

            
   
         $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' 		=> 'ectbe_form_typography_btn',
                'label' 	=> __( 'Typography', 'ectbe' ),
                'selector' 	=>   ' {{WRAPPER}} #rtec [name="rtec_submit"],
                   {{WRAPPER}} #rtec-form-toggle-button,
                   {{WRAPPER}} #ectbe_reg_form,
                   {{WRAPPER}} #rtec [name="rtec_visitor_submit"]',
            ]
        );
       
           $this->add_control(
			'ectbe_content_container_heading',
			[
				'label' => __( 'Container', 'ectbe' ),
				'type' => \Elementor\Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);
 

    

        $this->add_responsive_control(
            'ectbe_form_padding', //param_name
            [
                'label' 		=> __( 'Padding', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} #rtec' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'ectbe_form_margin', //param_name
            [
                'label' 		=> __( 'Margin', 'ectbe' ), //heading
                'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
                'size_units' 	=> [ 'px', 'em', '%' ],
                'selectors' 	=> [
                      '{{WRAPPER}} #rtec' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]
        );
        	$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' 		=> 'ectbe_form_box_shadow',
				'label' 	=> __('Box Shadow', 'ectbe'),
                'selector' 	=> '{{WRAPPER}} #rtec',
			]
		);


     

        $this->end_controls_section();

    }

    protected function render()
    {
        $css_class = 'ectbe-registration-form';

        $single_page_meta_event_map_op = '';
        $id = get_the_ID();
        if (Elementor\Plugin::$instance->editor->is_edit_mode()) {
            $post = get_posts('post_type=tribe_events&numberposts=1');
            $post = $post[0];
            $id = $post->ID;
            echo '<button id="ectbe_reg_form">Register ▽</button>';
            ?>
           <div class="ectbe-sidebar-box"  id="ectbe_reg_custom">            
                <div class="ectbe-registration-form <?php echo $css_class; ?>">
                    <?php
                    if (function_exists('rtec_the_registration_form')) {
                        echo rtec_the_registration_form();
                    }
                    ?>
                </div>
            </div>
            <?php            
        }
        else{
        $settings = $this->get_settings_for_display();
        $css_class = 'ectbe-registration-form';        
        ?>       
          <style>
          #ectbe-reg-custom1 .rtec-outer-wrap.rtec-js-placement{
              display:block ;
          }
          </style>
            <div class="ectbe-sidebar-box"  id="ectbe-reg-custom1">
            
                    <div class="ectbe-registration-form <?php echo $css_class; ?>">
                        <?php
                        if (function_exists('rtec_the_registration_form')) {
                            echo rtec_the_registration_form();
                        }
                    ?>
                     </div>
            </div>

        <?php
        }
        
        }

}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type(new Ectbe_widgets_registration_form());
