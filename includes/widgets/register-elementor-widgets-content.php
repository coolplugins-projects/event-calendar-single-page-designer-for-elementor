<?php
use Elementor\Widget_Base;
use Elementor\Controls_Manager;

use Elementor\Group_Control_Typography;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Box_Shadow;


class Ectbe_widgets_content extends \Elementor\Widget_Base {

    public function __construct($data = [], $args = null) {
        // must call the parent class constructor
        parent::__construct($data, $args);
  
    }

    public function get_categories(){
        return ['ectbe_ect_single_addons'];
    }

/*    public function get_script_depends() {
         return [ 'ectbe-content-widget' ];
     }
*/
    public function get_name(){
        return "the-event-content";
    }

    public function get_title(){
        return "Event Content";
    }

    public function get_icon(){
        return 'fa fa-paragraph';
    }

    // register controls
	protected function _register_controls() {

		$this->start_controls_section(
			'content_section',
			[
				'label' => __( 'Content Settings', 'ectbe' ),
				'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
			]
		);

        $this->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' 		=> 'content_typography',
					'label' 	=> __( 'Typography', 'ectbe' ),
					'selector' 	=> '{{WRAPPER}} .ectbe-event-content, {{WRAPPER}} .ectbe-event-content p,{{WRAPPER}} .ectbe-event-content h2',
				]
			);

			$this->add_control(
				'ectbe_content',
				[
					'label' 		=> __( 'Content Color', 'color' ),
					'type' 			=> \Elementor\Controls_Manager::COLOR, 
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-event-content, 
						{{WRAPPER}} .ectbe-event-content p,
						{{WRAPPER}} .ectbe-event-content h2' => 'color: {{VALUE}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'ectbe_content_padding', //param_name
				[
					'label' 		=> __( 'Padding', 'ectbe' ), //heading
					'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
					'size_units' 	=> [ 'px', 'em', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-event-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_responsive_control(
				'ectbe_content_margin', //param_name
				[
					'label' 		=> __( 'Margin', 'ectbe' ), //heading
					'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
					'size_units' 	=> [ 'px', 'em', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-event-content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}} !important;',
					],
				]
			);

			$this->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' 			=> 'ectbe_content_border',
					'label' 		=> __( 'Border', 'ectbe' ),
					'selector' 		=> '{{WRAPPER}} .ectbe-event-content',
				]
			);

			$this->add_control(
				'ectbe_title_shape_radius', //param_name
				[
					'label' 		=> __( 'Border Radius', 'ectbe' ), //heading
					'type' 			=> \Elementor\Controls_Manager::DIMENSIONS, //type
					'size_units' 	=> [ 'px', 'em', '%' ],
					'selectors' 	=> [
						'{{WRAPPER}} .ectbe-event-content' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);
	
			$this->add_group_control(
				Group_Control_Box_Shadow::get_type(),
				[
					'name' 		=> 'ectbe_title_box_shadow',
					'label' 	=> __('Box Shadow', 'ectbe'),
					'selector' 	=> '{{WRAPPER}} .ectbe-event-content',
				]
			);			

		$this->end_controls_section();

    }
    
    protected function render() {
        
        GLOBAL $post;
        if ( Elementor\Plugin::$instance->editor->is_edit_mode() ) { 
            $post = get_posts( 'post_type=tribe_events&numberposts=1' );
            $post = $post[0];
			    if(empty($post->post_content)){                
                echo'<span class="ectbe-alert-msg"><b>'. __('This widget is displayed if Content is set in this page.', 'ectbe').'</b></span>';

            } 
        }

        $content = '<div class="ectbe-event-content">';

        $content .= $post->post_content;

        $content .= '</div>';

        echo $content;
    }
}
\Elementor\Plugin::instance()->widgets_manager->register_widget_type( new Ectbe_widgets_content() );
