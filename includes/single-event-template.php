<?php
/**
 * 
 * This is a template file for single events
 * This template page can be utilize for costumizing the single events page
 * 
 */
get_header();
$single_template_id = get_option( 'ectbe_events_addons_single_page_id' , false );
?>
<div id="ectbe-main-content" class="ectbe-main-content">
<div id="primary" class="content-area">
<div id="content" class="site-content" role="main">
<style>
    .rtec-outer-wrap.rtec-js-placement {
            display:none;
        }
       
        </style>

<?php
$options = get_option('rtec_options');

$update=update_option('template_location','shortcode');

// Render post data with Elementor elements.
echo  \Elementor\Plugin::instance()->frontend->get_builder_content_for_display( $single_template_id ) 

?>
</div>
</div>
</div>