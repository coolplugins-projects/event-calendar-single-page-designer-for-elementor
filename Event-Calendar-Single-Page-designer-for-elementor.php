<?php
/**
 *
 *
 * Plugin Name: Event Calendar Single Page designer for elementor
 * Version: 1.0.0
 * Author: Cool Plugins
 * Description: This plugin is usefull in desigining The Event Calendar Single Events page with Elementor addons
 *
 */

//event-single-page-addons
define('ECTBE_PLUGIN_FILE', __FILE__);
define('ECTBE_PLUGIN_URL', plugin_dir_url(ECTBE_PLUGIN_FILE));
define('ECTBE_PLUGIN_DIR', plugin_dir_path(__FILE__));


class ectbe_single_page_template_el_addon
{

    public function __construct()
    {

        // required necessary files for the plugin
        $this->enqueue_required_files();

        // if (!did_action('elementor/loaded')) {
        //     add_action('admin_notices', array($this, 'admin_notice_missing_elementor_plugin'));
        // } else {
            add_action('admin_menu', array($this, 'ectbe_init_event_admin_menu'));
        // }

        add_action('admin_init', array($this, 'ectbe_customize_single_event_page'));

        add_action('init', array($this, 'ectbe_post_type'));

        // override default Event Calendar single page template
        add_filter('tribe_events_template_single-event.php', array($this, 'ectbe_tribe_events_single_event_page'), 200, 1);

        // initialize elementor addon
        add_action('elementor/widgets/widgets_registered', array($this, 'ectbe_required_widgets_files'));
        add_action('elementor/elements/categories_registered', array($this, 'ectbe_register_elementor_widget_categories'));

          
            register_activation_hook(ECTBE_PLUGIN_FILE, array($this,'ectbe_importTemplate'));

  

    }
    
    function ectbe_importTemplate()
{
    $filepath = ECTBE_PLUGIN_DIR . 'includes\widgets\template\singletemp.json';

    $fileContent = file_get_contents($filepath);
    $fileJson = json_decode($fileContent, true);

    $result = \Elementor\Plugin::instance()->templates_manager->import_template([
        'fileData' => base64_encode($fileContent),
        'fileName' => 'test.json',
    ]
    );

    if (empty($result) || empty($result[0])) {
        return;
    }

    update_post_meta($result[0]['template_id'], '_elementor_location', 'myCustomLocation');
    update_post_meta($result[0]['template_id'], '_elementor_conditions', ['include/general']);
}


    public function admin_notice_missing_elementor_plugin()
    {

        $link = admin_url('plugin-install.php?tab=plugin-information&plugin=elementor&TB_iframe=true&width=772&height=694');
        echo "<div style='padding:10px;' class='notice error-message message'>Please install & activate <a href='$link' class='thickbox open-plugin-details-modal'> Elementor</a> plugin</div>";

    }

    public function ectbe_init_event_admin_menu()
    {
        $single_template_id = get_option('ectbe_events_addons_single_page_id', false);
        add_submenu_page('cool-plugins-events-addon', 'Edit Single Page Template', 'Single Events Page Customization', 'manage_options', admin_url('/post.php?post=' . $single_template_id . '&action=elementor'), __return_false());
    }

    public function enqueue_required_files()
    {
        require_once plugin_dir_path(__FILE__) . 'admin/events-addon-page/events-addon-page.php';
        cool_plugins_events_addon_settings_page('events-addons', 'cool-plugins-events-addon', '📅 Events Addons For The Events Calendar');

        require_once plugin_dir_path(__FILE__) . 'includes/single-event-template-initializer.php';

    }

    public function ectbe_customize_single_event_page()
    {
        $single_page_id = get_option('ectbe_events_addons_single_page_id', false);

        if ($single_page_id == false || 'publish' != get_post_status($single_page_id)) {

            $event_post_data = array(
                'post_title' => 'Single Page Template',
                'post_type' => 'ectpse',
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_content' => "[cool-event-title tag='h2']
                [cool-event-content]
                ",
            );

            $post_id = wp_insert_post($event_post_data);
            update_option('ectbe_events_addons_single_page_id', $post_id);
        }

    }

    public function ectbe_post_type()
    {
        $labels = array(
            'name' => _x('Single Event Template', 'Post Type General Name', 'ectbe'),
            'singular_name' => _x('Single Event Templates', 'Post Type Singular Name', 'ectbe'),
            'menu_name' => __('Single Event Template', 'ectbe'),
            'name_admin_bar' => __('Single Event Templates', 'ectbe'),
            'archives' => __('Item Archives', 'ectbe'),
            'attributes' => __('Item Attributes', 'ectbe'),
            'parent_item_colon' => __('Parent Item:', 'ectbe'),
            'all_items' => __('Events Single Page Templates', 'ectbe'),

            'update_item' => __('Update Item', 'ectbe'),
            'view_item' => __('View Item', 'ectbe'),
            'view_items' => __('View Items', 'ectbe'),
            'search_items' => __('Search Item', 'ectbe'),
            'not_found' => __('Not found', 'ectbe'),
            'not_found_in_trash' => __('Not found in Trash', 'ectbe'),
            'featured_image' => __('Featured Image', 'ectbe'),
            'set_featured_image' => __('Set featured image', 'ectbe'),
            'remove_featured_image' => __('Remove featured image', 'ectbe'),
            'use_featured_image' => __('Use as featured image', 'ectbe'),
            'insert_into_item' => __('Insert into item', 'ectbe'),
            'uploaded_to_this_item' => __('Uploaded to this item', 'ectbe'),
            'items_list' => __('Items list', 'ectbe'),
            'items_list_navigation' => __('Items list navigation', 'ectbe'),
            'filter_items_list' => __('Filter items list', 'ectbe'),
        );
        $args = array(
            'label' => __('', 'ectbe'),
            'description' => __('Post Type Description', 'ectbe'),
            'labels' => $labels,
            'supports' => array('title', 'editor', 'author', 'elementor'),
            'taxonomies' => array(''),
            'hierarchical' => true,
            'public' => true, // it's not public, it shouldn't have it's own permalink, and so on
            'show_ui' => true,
            'show_in_menu' => false,
            'menu_position' => 5,
            'show_in_admin_bar' => true,
            'show_in_nav_menus' => true,
            'can_export' => true,
            'has_archive' => true, // it shouldn't have archive page
            'rewrite' => false, // it shouldn't have rewrite rules
            'exclude_from_search' => true,
            'publicly_queryable' => true,
            // 'menu_icon'           => ectbe_PLUGIN_URL.'/assets/images/pb-icon.png',
            'capability_type' => 'page',

            'map_meta_cap' => true,

        );
        register_post_type('ectpse', $args);
    }

    public function ectbe_tribe_events_single_event_page($template_file)
    {

        if (basename($template_file) != 'single-event.php') {
            return $template_file;
        }

        return plugin_dir_path(__FILE__) . '/includes/single-event-template.php';

    }

    public function ectbe_register_elementor_widget_categories($elements_manager)
    {
        $elements_manager->add_category(
            'ectbe_ect_single_addons',
            [
                'title' => 'Events Single Page Template ',
                'icon' => 'fa fa-plug',
            ]
        );

    }
    

    public function ectbe_required_widgets_files()
    {

        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-title.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-event-image.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-content.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-all-events.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-excerpt.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-schedule-details.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-ticket-cost.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-google-cal-export.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-meta-details.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-venue-details.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-event-organizers.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-related-events.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-event-footer.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-event-map.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-event-share-button.php';

       //require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-additional-fields.php';

        if (function_exists('tribe_get_custom_fields')) {
            // Additional fields addon for Elementor Pro
            require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-additional-fields.php';
        }

        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-event-countdown.php';

        if (class_exists('Registrations_For_The_Events_Calendar')) {
            require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-registration-form.php';
        }
         if (class_exists('Tribe__Tickets__Main')) {
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-rsvp-form.php';
        require_once plugin_dir_path(__FILE__) . '/includes/widgets/register-elementor-widgets-tickets.php';
         }


    }
}

new ectbe_single_page_template_el_addon();
