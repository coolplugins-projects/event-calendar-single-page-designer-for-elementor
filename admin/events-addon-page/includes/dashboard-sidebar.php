<?php

/**
 * 
 * Addon dashboard sidebar.
 */

 if( !isset($this->main_menu_slug) ):
    return false;
 endif;

 $cool_support_email = "mailto:contact@coolplugins.net";
?>

 <div class="cool-body-right">
    <img src="<?php echo plugin_dir_url( $this->addon_file ) .'/assets/coolplugins-logo.png'; ?>"><br/>Add more info here.<br/>
    <a href="<?php echo $cool_support_email; ?>" target="_new" class="button button-primary">Contact Support</a>
    </div>

</div><!-- End of main container-->